package com.company;

import java.util.Arrays;

/**
 * Created by federicobrubacher on 11/16/14.
 */
public class KnapsackIter {


    public static int[] itemWeights = { 3, 9, 2, 1};
    public static int[] itemValues = { 3, 12, 11, 9};
    static int n = 4;
    static int W = 10;

    static int[][] m = new int[n+1][W+1];




    public static void calculateKnapsack() {

        for (int row = 0; row <= n ; row++){
            Arrays.fill(m[row], 0);
        }

        for (int i = 1; i<= n; i++) {
            for (int j = 0; j <= W; j++) {
                if (itemWeights[i-1] <= j) {
                    m[i][j]  = Math.max(m[i-1][j], m[i-1][ j-itemWeights[i-1]] + itemValues[i-1]); //
                } else {
                    m[i][j] = m[i-1][j];
                }
            }
            if (i >= 1) {
                for (int[] rows : m) {
                    for (int col : rows) {
                        System.out.format("%5d", col);
                    }
                    System.out.println();
                }
            }
            System.out.println("=========================");
        }

        for (int[] rows : m) {
        for (int col : rows) {
            System.out.format("%5d", col);
        }
        System.out.println();
    }
}
    public static void main(String[] args) {
        calculateKnapsack();
        System.out.println("RESULTADO :" + m[4][10]);
    }
}
