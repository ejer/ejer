package com.company.dynamic;

import java.util.Arrays;
import java.util.List;

/**
 * Created by federicobrubacher on 6/1/15.
 */
public class MaxSubarray {
    // Algoritmo :
    // dado un número en una posición , fijarse si la inclusión del proximo número hace a la suma mas grande o no

    public static int recursiveSol(int currSum, List<Integer> input) {
        int max = 0;
        for (int i = 0; i < input.size(); i++) {

            List<Integer> subList = input.subList(1, input.size());
            max = Math.max(currSum + recursiveSol(currSum, subList), recursiveSol(0, subList));

        }
        return max;
    }

    public static void main(String[] args) {

        int[] input = {-2, 1, -3, 4, -1, 2, 1, -5, 4, 0};
        List<Integer> inputList = Arrays.asList(-2, 1, -3, 4, -1, 2, 1, -5, 4);

        int[] result = new int[input.length + 1];
        int max = 0;


        result[0] = 0;

//        int recursiveSol = recursiveSol(0, inputList);
//        System.out.print(recursiveSol);


        for (int i = 1; i < input.length - 1; i++) {
            if (result[i - 1] <= input[i - 1]) {
                result[i] = input[i - 1];

            } else {
                result[i] = result[i - 1] + input[i - 1];
            }
            if (result[i] > max)
                max = result[i];
        }

        System.out.println(max);
//

//        for (int i = 1; i <= input.length; i++) {
//            if ( result[i-1] < input[i-1]) {
//                result[i] = input[i-1];
//            } else if ( result[i-1] > input[i-1]) {
//                if (input[i-1] < 0) {
//                    result[i] = result[i - 1];
//                } else {
//                    result[i] = result[i-1];
//                }
//            } else {
//                result[i] = result[i-1];
//            }
//
//        }
//        for (int i = 0; i < input.length; i++) {
//
//            System.out.println(input[i] + " " + result[i]);
//        }

    }

}
