package com.company.dynamic;

/**
 * Created by federicobrubacher on 7/26/15.
 */
public class OnlineShopping {

    static int T = 2;
    static int N = 2;
    static int M = 2;
    static int[][] prices = {{3,1},{1,2}};
    static int[][] discounts = {{0,2}, {0,1}};

    static int size = prices[0].length;


    public static int total = 0;
    static int[] minItem = new int[M];

    public static int minAmont( int curr, int item, int prevStore) {

        if (item == size) {
            return curr;
        }


        int minPrice  = Integer.MAX_VALUE;

        for (int i = 0; i < M; i++) {

            int price =prices[item][i];
            int applyDiscount = 0;
            if (prevStore == i  && item>0)
            {
                applyDiscount = discounts[item-1][prevStore];

            }
            int purchaseStoreItem = minAmont(curr+price-applyDiscount, item+1, i);
            if (purchaseStoreItem < minPrice) {
                minPrice = purchaseStoreItem;
                minItem[item] = i;
            }

        }



        return minPrice;



    }

    public static int minAmountDyn() {

        int[][] dp = new int[N+1][M+1];
        for (int i = 0; i < M; i++) {
            dp[0][i] = 0;
        }

        /* N items con M coupons */
        /* la recursion es algo como :

        C[i][j] = C[i-1][j-1] + price - discount
         */

        int[][] price = new int[N][M];
        int[][] discounts = new int[N][M];

        for (int j = 1; j < N; j++) {

            int min = Integer.MAX_VALUE;
            for (int k = 0; k < M; k++) {
                if(min > dp[j-1][k]) {
                    min = dp[j-1][k];
                }

            }

            for (int k = 0; k < M; k++) {

                int disc  = price[j][k] - discounts[j-1][k];
                if (disc > 0)
                    disc = 0;
                dp[j][k] = Math.min(dp[j-1][k] + disc, price[j-1][k] + min );
            }
        }

        return dp[N][M];
    }

    public static void main(String[] args) {
        minAmont(0,0,0);

        for (int i = 0; i < M; i++) {
            System.out.format("%5d", minItem[i]);
            System.out.println();
        }
    }


}
