package com.company.dynamic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 1/27/15.
 */
public class LIncreasingS {

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }

    public static void main(String[] args) throws IOException {
        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/LincreasingS/1.txt")));
        Input2 in2 = new Input2();
        int result = increasingSeq(in3);
        System.out.println(result);
        }

    private static int increasingSeq(Input in3) throws IOException {
        int nbr = in3.nextInt();
        int[] elems = new int[nbr];
        for (int i = 0; i < nbr; i++) {
            elems[i] = in3.nextInt();
        }
        int[] m = new int[nbr + 1];
        m[0] = 1;
        int[] n = new int[nbr + 1];
        int currMax = 0;
        int currPred = 0;
        for (int i = 1; i < nbr; i++) {
            for (int j = 0; j < i; j++) {
                if (currMax < m[j]) {
                    if (elems[j] < elems[i]) {
                        currMax = m[j];
                        currPred = j;
                    }
                }
            }
            m[i] = currMax+1;
            n[i] = currPred;
        }

        return  m[nbr];
    }




}

