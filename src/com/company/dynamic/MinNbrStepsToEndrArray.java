package com.company.dynamic;

/**
 * Created by federicobrubacher on 8/1/15.
 */
public class MinNbrStepsToEndrArray {
    static int[] array = {1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9};

    static int[] cache =  new int[array.length];

    public static int minSteps(int i, int nbrSteps) {

        int size = array.length;

        if (i>=size-1) {
            return 0;
        }


        int min = Integer.MAX_VALUE;


        for (int j = i+1; j <= i + array[i] ; j++) {

            int curr = minSteps(j, nbrSteps + 1);
            if (min > curr) {

                min = curr;
            }

        }

        return min+nbrSteps;


    }

    public static int minJumps() {
        int[] jumps = new int[array.length];

        int n = array.length;
        jumps[0] = 0;

        for (int i = 1; i < n; i++) {
            // Find the minimum number of jumps to reach arr[i]
            // from arr[0], and assign this value to jumps[i]
            // ex j == 1 i == 3
            jumps[i] = Integer.MAX_VALUE;

            for (int j = 0; j < i; j++) {

                if (i <= j + array[j] && jumps[j] != Integer.MAX_VALUE)
                {
                    jumps[i] = Math.min(jumps[i], jumps[j]+1);
                    break;

                }


            }

        }
        return jumps[n-1];

    }

    public static void main(String[] args) {
        int steps = minSteps(0,0);
        int minJumps = minJumps();
        System.out.println(steps);
        System.out.println(minJumps);

    }

}
