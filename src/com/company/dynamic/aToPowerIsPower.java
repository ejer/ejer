package com.company.dynamic;

/**
 * Created by federicobrubacher on 7/31/15.
 */
public class aToPowerIsPower {

    /* can i be represented as 5^n?
    * 125 true
    *  120 false
    * */
    public static boolean isMultiple(int i) {

        if (i % 5 != 0) {
            return  false;
        }
        if (i == 5) {
            return true;
        }

        return isMultiple(i/5);

    }

    /* a to the power of b
    * if b is % 2
    * a^(b/2+b/2)
    *
    * a*caso1
    * */

    public static int atoPowerB(int a, int b) {

        if (b == 1) {
            return a;
        }
        if (b % 2 == 0) {
            return atoPowerB(a,b/2)*atoPowerB(a,b/2);
        } else {
            return a*atoPowerB(a,b-1);
        }
    };

    /* fibo with matrices */

    public static int fastFibo(int n){

        return 0;
    }



    public static void main(String[] args) {

        System.out.println(isMultiple(125));
        System.out.println(isMultiple(120));
        // 19683
        System.out.println(atoPowerB(3,9));

    }

}
