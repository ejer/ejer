package com.company.dynamic;

/**
 * Created by federicobrubacher on 12/5/15.
 */
public class BurstBalloons {
    public int burst(int[][] dp, int[] nums, int be, int en) {
        if (be + 1 == en) return 0;
        if (dp[be][en] > 0) return dp[be][en];
        int ans = 0;
        for (int i = be + 1; i < en; ++i) {
            int curr = nums[be] * nums[i] * nums[en];
            ans = Math.max(ans, curr + burst(dp, nums, be, i) + burst(dp, nums, i, en));
        }
        dp[be][en] = ans;

        return ans;

    }

    public int maxCoins(int[] iNums) {
        int[] nums = new int[iNums.length + 2];
        int n = 1;
        for (int x : iNums) if (x > 0) nums[n++] = x;
        nums[0] = nums[n++] = 1;


        int[][] memo = new int[n][n];
        return burst(memo, nums, 0, n - 1);

    }

    public int maxCoinsDP(int[] iNums) {
        int[] nums = new int[iNums.length + 2];
        int n = 1;
        for (int x : iNums) if (x > 0) nums[n++] = x;
        nums[0] = nums[n++] = 1;


        int[][] dp = new int[n][n];
        for (int k = 2; k < n; ++k)
            for (int left = 0; left < n - k; ++left) {
                int right = left + k;
                for (int i = left + 1; i < right; ++i) {
                    dp[left][right] = Math.max(dp[left][right],
                            nums[left] * nums[i] * nums[right] + dp[left][i] + dp[i][right]);
                    for (int[] rows : dp) {
                        for (int col : rows) {
                            System.out.format("%5d", col);
                        }
                        System.out.println();
                    }
                    System.out.println("------------------------");
                }
            }

        return dp[0][n - 1];
    }

    public static void main(String[] args) {
        BurstBalloons burstBalloons = new BurstBalloons();
        int[] nums = {3, 1, 5, 8};
        int i = burstBalloons.maxCoinsDP(nums);

        assert i == 167;

    }
}
