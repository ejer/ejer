package com.company.dynamic;

/**
 * Created by federicobrubacher on 1/10/15.
 */
public class PartitionProblem {
    static int MAXN = 30;
    static int MAXK = 3;

    // min max of M[i, k-1] , sum i+1 n of sj
    public static int partition(int input[], int n, int k) {

        int[][] m = new int[n + 1][k + 1];
        int[][] d = new int[n + 1][k + 1];
        int p[] = new int[n + 1];
        int cost;
        int i, j, x;
        p[0] = 0;
        for (i = 1; i <= n; i++) p[i] = p[i - 1] + input[i];

        for (i = 1; i <= n; i++) m[i][1] = p[i];
        for (j = 1; j <= k; j++) m[1][j] = input[1];

        for (i = 2; i <= n; i++)
            for (j = 2; j <= k; j++) {
                m[i][j] = Integer.MAX_VALUE;
                for (x = 1; x <= (i - 1); x++) {
                    cost = Math.max(m[x][j - 1], p[i] - p[x]);
                    if (m[i][j] > cost) {
                        m[i][j] = cost;
                        d[i][j] = x;
                    }
                }
            }

        for( int pi : p) {
            System.out.print( pi);
        }
        System.out.println();
        for (int[] rows : m) {
            for (int col : rows) {
                System.out.format("%5d", col);
            }
            System.out.println();
        }
//        for (int[] rows : d) {
//            for (int col : rows) {
//                System.out.format("%5d", col);
//            }
//            System.out.println();
//        }
        reconstruct_partition(input, d, n, k);
        return m[n][k];
    }

    public static void reconstruct_partition(int[] input, int[][] d, int n, int k) {

        if (k == 1) {
            print_books(input, 1, n); // fijarse que el n es ahora el end en la rutina print_books
        } else  {
            reconstruct_partition(input, d,d[n][k],k-1);
            System.out.println("---");
            print_books(input,d[n][k]+1,n);
        }

    }

    private static void print_books(int[] input, int start, int end) {

        for (int i = start; i <=end; i++) {
            System.out.println("Book " + input[i]);

        }
    }

    public static void main(String[] args) {

        int[] input = {0, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        int[] input2 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int result = partition(input2, 9, 3);
        System.out.println(result);
    }
}
