package com.company.dynamic;

/**
 * Created by federicobrubacher on 7/26/15.
 */
public class SpidermanJumping {

    /* from building i to j iff i < j and j-i is a power of 2
    1,2,4 Height[j]-Height[i] where Height[i]
     */
    static int N = 4;
    static int[] heights = {1, 2, 3, 4};
    static int total = 0;

    public static int minEnergy(int i, int currSum) {

        if (i == N) {
            return currSum;
        }

        int min = Integer.MAX_VALUE;
        for (int j = i+1; j < N+1; j++) {

            int energy = heights[j-1] - heights[i-1];
            if ( energy > 0 && ( (j-i) % 2 == 0  || (j-i) == 1)) {

                int itoN = minEnergy(j, energy+currSum);
                if (itoN < min) {
                    min = itoN;
                }

            }


        }


        return min;


    }


    public static void main(String[] args) {


        int sum = minEnergy(1, 0);

        System.out.print(sum);

    }
}

