package com.company.dynamic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicobrubacher on 9/17/15.
 */
public class SquareDivisors {

    public int numHelper(int n, Integer[] squares, int pos) {

        if (n == 0) return 0;

        if (n < 0) return Integer.MIN_VALUE;

//        return Math.min(numHelper(n-squares[pos], squares, pos-1), numHelper(n, squares, ))

        return 0;
    }



    public int numSquares(int n) {

        if (n == 0) return 0;

        List<Integer> ll = new ArrayList<Integer>();

        for (int i = 0; i*i <= n; i++) {
            ll.add(i*i);
        }

        Integer[] squares = new Integer[ll.size()];
        ll.toArray(squares);
        return numHelper(n, squares,squares.length-1);


    }

}
