package com.company.dynamic;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 1/23/15.
 */
public class KnapsackHackerRank {
    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }

    public static void main(String[] args) throws IOException {
        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/Knapsack/in.txt")));
        Input2 in2 = new Input2();
        int nbrExamples = in3.nextInt();
        for (int i = 0; i < nbrExamples; i++) {
            int result = knapMultiSet(in3);
            System.out.println(result);
        }



    }

    private static int knapMultiSet(Input in3) throws IOException {
        int nbrElems = in3.nextInt();
        int max = in3.nextInt();
        List<Integer> elems  = new ArrayList<Integer>();
        for (int i = 0; i < nbrElems; i++) {
            int elem = in3.nextInt();
            elems.add(i,elem);
            int times = 2;
            while (elem*times <= max) {
                elems.add(elem*times);
                times+=1;
            }
        }
        int m[][] = new int[elems.size()+1][max+1];

        for (int[] ints : m){

            Arrays.fill(ints,0);
        }

        for (int i = 1; i <= elems.size(); i++) {
            for (int j = 0; j <= max; j++) {
                if (elems.get(i - 1) <= j) {
                    // Aca va si uso o no uso el curr elem
                    // Lo interesante es ver que me fijo cual es mas grande,
                    // la suma parcial anterior (sin el curr elem) [i-1,j] o con el curr elem
                    // pero para incluir el curr elem necesitamos ver como es el fit entonces vemos j-elem[i] lugares como es
                    // la suma  y es es el valor a comparar con la suma parcial anterior
                    m[i][j] = Math.max(m[i - 1][j], m[i - 1][j - elems.get(i - 1)] + elems.get(i - 1)); //
                } else {
                    m[i][j] = m[i - 1][j];
                }
            }

        }
//        for (int[] rows : m) {
//            for (int col : rows) {
//                System.out.format("%5d", col);
//            }
//            System.out.println();
//        }

        return m[elems.size()][max];
    }
}
