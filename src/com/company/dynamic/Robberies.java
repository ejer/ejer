package com.company.dynamic;

/**
 * Created by federicobrubacher on 8/31/15.
 */
public class Robberies {

    public static int rob(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        if (nums.length == 1) {
            return nums[0];
        }
        cache = new int[nums.length];

        for (int i = 0 ; i < cache.length; i++) {
            cache[i] = -1;
        }
        return rob(nums, 0, false);

     /*
        int[] amount = new int[nums.length];
        boolean[] isVisited = new boolean[nums.length];

        for (int i = 1; i < nums.length; i++) {

            amount[i] = Math.max(
                amount[i-1] + nums[i-1]

                )

        }

        return amount[nums.lenght-1];
       */
    }


    public static int[] cache = null;

    public static int rob(int[] nums, int i, boolean isPrevRobbed) {

        if ( i == nums.length) {
            return cache[i-1];
        }

        if (cache[i] != -1) {
            return cache[i];
        }

        if (isPrevRobbed) {
            cache[i] = rob(nums, i+1, false);
        }

        return cache[i] = Math.max(
                nums[i] + rob(nums, i+1, true),
                rob(nums, i+1, false));
    }

    public static void main(String[] args) {

        int[] p = {1, 1};
        int rob = rob(p);
        System.out.print(rob);
    }
}
