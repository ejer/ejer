package com.company.dynamic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by federicobrubacher on 4/17/15.
 */
public class KnapsackCoursera {

    public static int[] weights;
    public static int[] values;

    static int max_weight = 5;
    static int[][] m;
    static int W;
    static int n;


    public static class Pair<A, B> implements java.lang.Comparable {
        private A first;
        private B second;

        public Pair(A first, B second) {
            super();
            this.first = first;
            this.second = second;
        }

        public int hashCode() {
            int hashFirst = first != null ? first.hashCode() : 0;
            int hashSecond = second != null ? second.hashCode() : 0;

            return (hashFirst + hashSecond) * hashSecond + hashFirst;
        }

        public boolean equals(Object other) {
            if (other instanceof Pair) {
                Pair otherPair = (Pair) other;
                return
                        ((this.first == otherPair.first ||
                                (this.first != null && otherPair.first != null &&
                                        this.first.equals(otherPair.first))) &&
                                (this.second == otherPair.second ||
                                        (this.second != null && otherPair.second != null &&
                                                this.second.equals(otherPair.second))));
            }

            return false;
        }

        public String toString() {
            return "(" + first + ", " + second + ")";
        }

        public A getFirst() {
            return first;
        }

        public void setFirst(A first) {
            this.first = first;
        }

        public B getSecond() {
            return second;
        }

        public void setSecond(B second) {
            this.second = second;
        }

        @Override
        public int compareTo(Object o) {
            Pair o1 = (Pair) o;
            if (this.first == o1.first && this.second == o1.second) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {


        Scanner input = new Scanner(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera2/knapsack_big.txt")));
        W = input.nextInt();
        n = input.nextInt();
        weights = new int[n];
        values = new int[n];
        for (int i = 0; i < n; i++) {
            values[i] = input.nextInt();
            weights[i] = input.nextInt();
        }
//        m = new int[n + 1][W + 1];

//        int result = calculateKnapsack();
//        System.out.println(result);
        int result2 = knapsack(W, 0);
        System.out.println(result2);


    }


//    public static int calculateKnapsack() {
//
//        for (int row = 0; row <= n; row++) {
//            Arrays.fill(m[row], 0);
//        }
//
//        for (int i = 1; i <= n; i++) {
//            for (int j = 0; j <= W; j++) {
//                if (weights[i - 1] <= j) {
//                    int prev_value = m[i - 1][j];
//                    int if_we_include_curr = m[i - 1][j - weights[i - 1]] + values[i - 1];
//                    m[i][j] = Math.max(prev_value, if_we_include_curr); //
//                } else {
//                    m[i][j] = m[i - 1][j];
//                }
//            }
//        }
//
//        for (int[] rows : m) {
//            for (int col : rows) {
//                System.out.format("%5d", col);
//            }
//            System.out.println();
//        }
//        return m[n][W];
//    }


//    static TreeMap<Pair, Integer> cache = new TreeMap<Pair, Integer>();

    static LRUCache cache = new LRUCache(5000000);


    private static int knapsack(int curr_weight, int i) throws FileNotFoundException {
        if (i == weights.length)
            return 0;
        if (curr_weight < weights[i]) {
            return 0;
        } else {
            int max1, max2;
            Pair currPair = new Pair(curr_weight, i + 1);
            max2 = cache.get(currPair);
            if (max2 == -1) {
                max2 = knapsack(curr_weight, i + 1);
                cache.set(currPair, max2);
            }
            Pair currPair2 = new Pair(curr_weight - weights[i], i + 1);
            max1 = cache.get(currPair2);
            if (max1 == -1) {
                max1 = knapsack(curr_weight - weights[i], i + 1) + values[i];
                cache.set(currPair2, max1);
            }
            System.out.println(i);

            return Math.max(max1, max2);
        }
    }

}
