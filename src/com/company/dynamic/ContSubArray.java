package com.company.dynamic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 1/21/15.
 */
public class ContSubArray {

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    private static void subArray(Input in) throws IOException {
        int nbrExamples = in.nextInt();
        for (int i=0; i < nbrExamples; i++){
            nbrElems = in.nextInt();
            int[] elems = new int[nbrElems];
            for (int j=0;j<nbrElems;j++) elems[j] = in.nextInt();
            overallMax = elems[nbrElems-1];
//            int elems[] = string2ArrayInts(in.next(), nbrElems);
//            printMaxSubSum(elems, 0, 0);
            int regreso = printMaxSubSum2(elems, nbrElems);
            int maxSum = printMaxSum(elems);
            Arrays.sort(elems);
            maxSum = maxSum > 0 ? maxSum : elems[nbrElems-1];
            regreso = regreso > 0 ? regreso : elems[nbrElems-1];
//            overallMax = overallMax > 0 ? overallMax : elems[nbrElems-1];
            System.out.println(regreso + " " + maxSum);
        }

    }

    public static void main(String[] args) throws IOException{
        String loco = "3 \n"
                + "4 \n"
                + "1 2 3 4 \n"
                + "6\n"
                + "-1 -2 -3 -4 -5 -6\n"
                + "6\n"
                + "2 -1 1 1 1 -5\n";
        Input in  = new Input(loco);

        Input2 in2 = new Input2();
        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/subArray/input01.txt")));

        subArray(in3);
    }

    private static int printMaxSum(int elems[])
    {
        int sum = 0;
        for (int i=0; i<nbrElems; i++)
            if (elems[i] > 0)
                sum+=elems[i];
        return sum;
    }


    static int nbrElems  = 0;
    static int overallMax = Integer.MIN_VALUE;

    private static int printMaxSubSum(int[] elems, int currIndex, int currSum) {
        if (currIndex == nbrElems ){
            return currSum;
        }
        int max = Math.max(currSum, printMaxSubSum(elems, currIndex+1, currSum+elems[currIndex]));
        if (max > overallMax) overallMax = max;
        return max;

    }

    private static int printMaxSubSum2(int[] elems, int nbrElems) {

        int m[][]  = new int[nbrElems+1][2];
        m[0][0] = 0; // keep before
        m[1][0] = 0; // sum new
        for (int i = 0; i < elems.length; i++) {
            if (elems[i] < 0)
                m[i+1][0] = 0;
            else
                m[i+1][0] = m[i][0]+elems[i];

            m[i+1][1] = Math.max(m[i+1][0], m[i][1]); // decido si quedarme con el maximo anterior o con la nueva suma parcial
            }
        for (int[] rows : m) {
            for (int col : rows) {
                System.out.format("%5d", col);
            }
            System.out.println();
        }
        return m[nbrElems][1];
    }
}
