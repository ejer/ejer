package com.company.dynamic;

/**
 * Created by federicobrubacher on 9/9/15.
 */
public class IsMatch {

    public static void main(String[] args) {
        String a = "abefcdgiescdfimde";
        String b = "ab*cd?i*de";

        boolean isMatch = isMatch(a, b);
    }

    private static boolean isMatch(String a, String b) {
        return isMatchHelper(a, b, 0, 0);
    }

    public static boolean isMatchHelper(String s, String p, int i, int j) {

        if (s.length() == 0 || p.length() == 0) return false;
        if (s.length() < p.length()) return false;
        if (p.length() == 1 && p.charAt(0) == '*') return true;
        if (s.length() == i) return true;
        if (s.length() > i && p.length() == j) return false;

        if (j >= 1) {

            if (p.charAt(j-1) == '*' && p.charAt(j)  != s.charAt(i)) {
                return isMatchHelper(s,p,i+1,j);
            }

        }

        if (s.charAt(i) == p.charAt(j) || p.charAt(j) == '*' || p.charAt(j) == '?') {
            return isMatchHelper(s,p,i+1,j+1);
        }

        return false;

    }

}
