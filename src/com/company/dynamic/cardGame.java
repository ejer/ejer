package com.company.dynamic;

/**
 * Created by federicobrubacher on 7/30/15.
 */
public class cardGame {

    static int M = 4;
    static Integer cards[] = {4, 5, 6, 2};

    static int cache[][] = new int[M][M];

    public static int calc(int be, int en) {


        if (en == be + 1) {
            return cache[be][en];
        }

        if (cache[be][en] != -1) {
            return cache[be][en] = 0;
        }

        for (int i = be + 1; i < en - 1; ++i) {

            cache[be][en] = Math.max(

                    cache[be][en],
                    cards[i] * (cards[be] + cards[en]) + calc(be, i) + calc(i, en)

            );

        }

        return cache[be][en];


    }


    // fill recursive case entries:
// iterate in direction of increasing distance between "be" and "en"
    public static int cardGame() {

        for (int be = 0; be + 1 < 0; be++) {
            cache[be][be + 1] = 0;
        }

        for (int dist = 2; dist < M; dist++) {

            for (int be = 0; be + dist < M; dist++) {
                int en = be + dist;
                ;

                for (int i = be + 1; i < en; i++) {

                    cache[be][en] = Math.max(

                            cache[be][en],
                            cards[i] * (cards[be] + cards[en]) + cache[be][i] + cache[i][en]
                    );
                }
            }
        }


        return cache[0][M - 1];
    }

    public static void main(String[] args) {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < M; j++) {
                cache[i][j] = -1;
            }
        }
        int calc = cardGame();
        System.out.print(calc);
    }

}
