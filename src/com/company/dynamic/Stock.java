package com.company.dynamic;

import java.util.Scanner;

public class Stock {

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }


    static int[][] dp = new int[3][3];

    public static int maxProfit2(int[] prices, int maxDays) {

        for (int i = 1; i < maxDays; i++) {
            for (int j = 0; j < maxDays; j++) {
                int doNothing;
                int buyOne = 0;
                int sellAll = 0;
                doNothing = dp[i - 1][j];
                if (j -1 >= 0)
                    buyOne = dp[i - 1][j - 1] - prices[i];
                if (j+1 < 3)
                    sellAll = dp[i - 1][j + 1] + prices[i];
                if (buyOne > doNothing && buyOne > sellAll) {
                    dp[i][j] =  buyOne;
                } else if (doNothing > buyOne && doNothing > sellAll) {
                    dp[i][j] =  doNothing;

                } else {
                    dp[i][j] =  sellAll;
                }
            }
        }
        return dp[2][2];
    }

    public static int maxProfit(int[] prices, int maxDays, int i, int sharesOwn) {
        if (i == maxDays) {
            return 0;
        }

        int buyOne = maxProfit(prices, maxDays, i + 1, sharesOwn + 1) - prices[i];
        int doNothing = maxProfit(prices, maxDays, i + 1, sharesOwn);
        int sellAll = maxProfit(prices, maxDays, i + 1, 0) + prices[i] * sharesOwn;

        if (buyOne > doNothing && buyOne > sellAll) {
            return buyOne;
        } else if (doNothing > buyOne && doNothing > sellAll) {
            return doNothing;

        } else {
            return sellAll;
        }
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */


        int testCases = 1;

        dp[0][0] = 0;
        for (int i = 0; i < 3; i++) {
            dp[0][i] = 0;
        }
        for (int i = 0; i < testCases; i++) {
            int[] prices = {1, 3, 1, 2};
            System.out.println(maxProfit2(prices, 3));
            // int[] prices2 = {1,2,100};
            // System.out.println(maxProfit(prices2, prices2.length, 0, 0));

        }

    }
}