package com.company.dynamic;

/**
 * Created by federicobrubacher on 7/31/15.
 */
public class WinesN {

    /*

    Imagine you have a collection of N wines placed next to each other on a shelf. For simplicity, let's number the wines from left to right as they are standing on the shelf with integers from 1 to N, respectively. The price of the i-th wine is pi (prices of different wines can be different).

Because the wines get better every year, supposing today is the year 1, on year y the price of the i-th wine will be y*pi, i.e. y-times the value that current year.

You want to sell all the wines you have, but you want to sell exactly one wine per year, starting on this year. One more constraint - on each year you are allowed to sell only either the leftmost or the rightmost wine on the shelf and you are not allowed to reorder the wines on the shelf (i.e. they must stay in the same order as they are in the beginning).

You want to find out, what is the maximum profit you can get, if you sell the wines in optimal order.
     */

    static int N = 6;
    static int prices[] = {10, 12, 22, 44, 18, 15};

    static public int sell(int be, int en,  int y) {

        if (be == en) {
            return y*prices[be];
        }

        return Math.max (

                sell(be+1, en, y+1) + y*prices[be],
                sell(be, en-1, y+1) + y*prices[en]


        );


    }

    static public int sell(int be, int en) {

        int y = prices.length - (en -be);

        if (be == en) {
            return y * prices[be];
        }

        return Math.max(

                sell(be + 1, en, y + 1) + y * prices[be],
                sell(be, en - 1, y + 1) + y * prices[en]


        );


    }


    static  int[][] cache = new int[prices.length][prices.length];

    static public int sell2(int be, int en) {

        if (be>en) {
            return 0;
        }

        if(cache[be][en] > 0) {
            return cache[be][en];
        }

        int y = prices.length - (en -be);

        return cache[be][en] = Math.max(

                sell2(be + 1, en) + y * prices[be],
                sell2(be, en - 1) + y * prices[en]

        );


    }

    public static void main(String[] args) {
        int sell0 = sell(0, prices.length - 1, 1);
        int sell1 = sell(0, prices.length - 1);
        int sell2 = sell2(0, prices.length - 1);


        System.out.println(sell0);
        System.out.println(sell1);
        System.out.println(sell2);

    }

}
