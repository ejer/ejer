package com.company;

/**
 * Created by federicobrubacher on 11/8/14.
 */
public class LCS {

    public static final String fede = "hola xxx";
    public static final String fede2 = "carola";

    public static int LCS(int agg, String word1, String word2){
        int index1 = word1.length()-1;
        int index2 = word2.length()-1;
        if (index1>= 0 && index2 >= 0 ) {
            if (word1.charAt(index1) == word2.charAt(index2)){
                System.out.println("Char at " + word1.charAt(index1));
                return LCS(agg+1, word1.substring(0, index1-1), word2.substring(0, index2-1));
            }
            else {
                return Math.max(LCS(agg, word1.substring(0, index1-1), word2), LCS(agg+1, word1, word2.substring(0, index2-1)));
            }
        }
        else {
           return agg;
        }

    }

    public static void main(String[] args) {
        System.out.println("RESULTADO :" + LCS(0, fede, fede2));
    }

}
