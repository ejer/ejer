package com.company;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;

/**
 * Created by federicobrubacher on 10/31/14.
 */
public class Main2 {


//    static int[] list = new int[] { 1, 1, -2, 9, -20, 9, -100};
    static int[] list = new int[] { 1, 9, -9, 1};
        // F[i][j] = max(F[i][j], F[i - 1][j] + W[A[i]])
   // if you remove the segment started at A[i] then we have: F[i + S[A[i]]][j + 1] = max(F[i + S[A[i]]][j + 1],

    // [1 1 -1 1 -1]
    static int t = 0;

    public static int dfs(int it, int agg) {
        int currValue = list[it];
        it = it + 1;
        if (it == list.length)
        {
            return max(agg, agg + currValue);
        } else {
            return max(agg, dfs(it, agg) +  currValue);
        }

    }

    static int K = 1;
//
    public static int dfs2(int it, int agg, int j) {
        int currValue = list[it];
        it = it + 1;
        int newValue =  agg + currValue;
        if (it == list.length)
        {
            System.out.println("hola1 " + newValue );
            return max(agg, newValue);
        } else {
            if (j < K) {
                System.out.println("hola3 " + agg);
                j = j +1;
                return max(dfs2(it, agg + newValue, j), dfs2(it, agg, j-1));
            }

            System.out.println("hola2 " + agg);
            return dfs2(it, newValue, j);
        }

    }

   public static void main(String[] args) {
      int result = dfs(0, 0);
      int result2 = dfs2(0, 0, 0);
      System.out.println("Resultado es :" + result);
       System.out.println("Resultado2 es :" + result2);
   }

}
