package com.company.Tree;

/**
 * Created by federicobrubacher on 9/3/15.
 */
public class ValidateBinarySearch {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public boolean isValidBST(TreeNode root) {

        if (root == null) return true;
        boolean leftIs = true;
        boolean rightIs = true;

        if (root.right != null) {
            rightIs = isValidBSTR(root.right, root.val);
        }
        if (root.left != null) {

            leftIs = isValidBSTL(root.left, root.val);
        }


        return leftIs && rightIs;
    }

    public boolean isValidBSTR(TreeNode node, int val) {
        if (val >= node.val) return false;
        boolean leftIs = true;
        boolean rightIs = true;
        if (node.right != null) {
            rightIs = isValidBSTR(node.right, val);
        }
        if (node.left != null) {

            leftIs = isValidBSTL(node.left, val);
        }

        return leftIs && rightIs;
    }

    public boolean isValidBSTL(TreeNode node, int val) {
        if (val <= node.val) return false;
        boolean leftIs = false;
        boolean rightIs = false;
        if (node.right != null) {
            rightIs = isValidBSTR(node.right, val);
        }
        if (node.left != null) {

            leftIs = isValidBSTL(node.left, val);
        }

        return leftIs && rightIs;
    }


    public static void main(String[] args) {

        ValidateBinarySearch vbs = new ValidateBinarySearch();

        //[10,5,15,null,null,6,20]
        TreeNode n10 = new TreeNode(10);

        TreeNode n5 = new TreeNode(5);
        TreeNode n15 = new TreeNode(15);
        TreeNode n6 = new TreeNode(6);
        TreeNode n20 = new TreeNode(20);


        n10.left = n5;
        n10.right = n15;
        n15.left = n6;
        n20.right = n20;
        boolean validBST = vbs.isValidBST(n10);

        System.out.print(validBST);
    }


}
