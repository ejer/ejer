package com.company.Tree;

import java.util.*;

/**
 * Created by federicobrubacher on 8/20/15.
 */
public class ZigZag {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }



    static public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        Deque<TreeNode> q1 = new LinkedList<>();
        Deque<TreeNode> q2 = new LinkedList<>();
        List<List<Integer>> lists = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        q1.offer(root);
        int counter = 0;
        boolean invert = true;
        while (true) {


            TreeNode node = q1.poll();
            if (node == null) {
                Deque<TreeNode> temp = new LinkedList<>(q1);

                if (q1.size() == 0) {
                    if (list.size() > 0) {
                        lists.add(list);
                    }
                    list = new ArrayList<>();
                }

                if (invert) {
                    invert = false;
                    while (q2.peek() != null) {
                        q1.offerFirst(q2.poll());
                    }
                } else {
                    q1 = q2;
                    invert = true;
                }
                q2 = temp;

                node = q1.poll();

                if (node == null) {
                    break;
                }

            }

            if (node.val != Integer.MIN_VALUE) {
                list.add(node.val);
            }


            if (node.left != null) {
                q2.offer(node.left);
            } else if (node.right != null) {
                counter++;
                q2.offer(new TreeNode(-1));
            }

            if (node.right != null) {
                q2.offer(node.right);
            } else if (node.left != null) {
                counter++;
                q2.offer(new TreeNode(Integer.MIN_VALUE));
            }

            if (counter % 2 != 0) {
                invert = !invert;
            }

        }
        return lists;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode leaf = new TreeNode(2);
        TreeNode leaf1 = new TreeNode(3);
        root.left = leaf;
        root.right = leaf1;
        leaf.left = new TreeNode(4);
        leaf1.right = new TreeNode(5);
        List<List<Integer>> lists = zigzagLevelOrder(root);
        System.out.print("hola");
    }

}
