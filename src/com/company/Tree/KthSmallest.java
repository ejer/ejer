package com.company.Tree;

import java.util.Stack;

/**
 * Created by federicobrubacher on 9/1/15.
 */
public class KthSmallest {
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public static int kthSmallest(TreeNode root, int k) {
        Stack<TreeNode> s = new Stack<>();

        if (root == null) {
            return -1;
        }

        if (k == 1) {
            return root.val;
        }

        TreeNode node = root;

        while(s.size() > 0 || node == root) {

            if (node != root) {
                node = s.pop();
                k--;
            }

            if ( k == 0 ) {
                return node.val;
            }

            if (node.left !=  null) {
                s.push(node.left);
            } else {
                s.push(node);

                if (node.right != null) {

                    s.push(node.right);

                }
            }


            node = null;



        }

        return -1;


    }

    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        n1.left = null;
        n1.right = new TreeNode(2);
        int i = kthSmallest(n1, 2);
        System.out.print(i);
    }
}
