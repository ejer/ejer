package com.company.Tree;


/**
 * Created by federicobrubacher on 8/15/15.
 */
public class LowestCommonAncestor {

    public class Node<T> {
        T value;
        Node<T> left;
        Node<T> right;
    }

    public static Node findLCA(Node root, Node node, Node node2)
    {

        /* los casos son :
        1) si uno de ellos es el root -> LCA
        2) si un nodo esta a la izq y otro nodo esta a la derecha -> LCA
        3) si estan del mismo go down tree to LCA
         */

        if (root == node || root == node2) {
            return root;
        }

        boolean node1Left = isParent(root.left, node);
        boolean node2Left = isParent(root.left , node2);
        if (node1Left && !node2Left || (!node1Left && node2Left)) {
            return root;
        }

        if (node1Left) {
           return findLCA(root.left, node, node2);
        } else {
            return findLCA(root.right, node, node2);
        }

    }

    public static boolean isParent(Node root, Node node) {

        if (root == null) {
            return false;
        }

        if (root == node) {
            return true;
        }


        boolean isLeft = isParent(root.left, node);
        boolean isRight = isParent(root.right, node);

        if (isLeft || isRight) {
            return true;
        }

        return false;


    }

    public static void main(String[] args) {


    }
}
