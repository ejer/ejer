package com.company.Tree;

import java.util.*;

/**
 * Created by federicobrubacher on 8/19/15.
 */
public class LevelTransversal {


    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }






    /*

    3
   / \
  9  20
 /  /  \
8   15   7

    c.offer(3)
    c.pop // 3
    p.offer 9, 20
    p.pop // 9
    c.offer 8
    c.pop 8
    p.pop 20
    c.offer 15, 7
     */


    public static List<List<Integer>> levelOrder(TreeNode root) {
        Queue<TreeNode> previous = new LinkedList<>();
        Queue<TreeNode> current = new LinkedList<>();
        List<List<Integer>> lists = new ArrayList<List<Integer>>();
        current.offer(root);
        ArrayList<Integer> list = new ArrayList<Integer>();
        while (true) {
            TreeNode node = current.poll();


            if (node == null) {
                Queue<TreeNode> tmp = previous;

                previous = current;
                current = tmp;
                node = current.poll();
                if (node == null) {
                    if (list.size() > 0){
                        lists.add(list);
                    }

                    break;
                }
            }

            list.add(node.val);

            if (current.size() == 0) {
                lists.add(list);
                list = new ArrayList<>();
            }


            if (node.left != null) {
                previous.offer(node.left);
            }

            if (node.right != null) {
                previous.offer(node.right);
            }


        }
        return lists;
    }

    private static void swap(Queue<TreeNode> previous, Queue<TreeNode> current) {

    }

    public static void main(String[] args) {

        TreeNode root = new TreeNode(1);
        TreeNode leaf = new TreeNode(2);
        root.right = leaf;
        levelOrder(root);
    }
}
