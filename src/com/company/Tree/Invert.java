package com.company.Tree;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by federicobrubacher on 8/26/15.
 */
public class Invert {


    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    static Deque<TreeNode> first = new LinkedList<>();
    static Deque<TreeNode> firstInverted = new LinkedList<>();
    static Deque<TreeNode> second = new LinkedList<>();
    static Deque<TreeNode> secondInverted = new LinkedList<>();

    public static TreeNode invertTree(TreeNode root) {


        if (root != null) {
            first.offer(root);
        }

        while (!first.isEmpty()) {
            TreeNode node = first.poll();

            if (node.left != null) {
                first.offer(root.left);
            }

            if (node.right != null) {
                first.offer(root.right);
            }

            TreeNode tmp = node.left;
            node.left = node.right;
            node.right = tmp;


        }

        return root;

    }



    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode leaf = new TreeNode(2);
        TreeNode leaf1 = new TreeNode(7);
        TreeNode leaf2 = new TreeNode(1);
        TreeNode leaf3 = new TreeNode(3);
        TreeNode leaf4 = new TreeNode(6);
        TreeNode leaf5 = new TreeNode(9);

        root.left = leaf;
        root.right = leaf1;
        leaf.left = leaf2;
        leaf.right = leaf3;
        leaf1.left = leaf4;
        leaf1.right = leaf5;

        invertTree(root);

        System.out.print("hello");
    }
}
