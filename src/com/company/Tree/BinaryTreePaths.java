package com.company.Tree;

import java.util.*;

/**
 * Created by federicobrubacher on 8/29/15.
 */
public class BinaryTreePaths {


    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }


    public static List<String> binaryTreePaths(TreeNode root) {

        List<String> list = new ArrayList<>();

        Stack<Deque<TreeNode>> s = new Stack<>();

        Deque<TreeNode> q = new LinkedList<>();
        q.offer(root);
        s.push(q);

        while (s.size() > 0) {

            Deque<TreeNode> q1 = s.pop();
            TreeNode node = q1.peekLast();


            if (node.left == null && node.right == null) {
                // it's a leaf don't add to the queue
                String path = constructString(q1);
                list.add(path);
            } else {

                if (node.right != null) {
                    s.push(addQueue(node.right,q1));
                }

                if (node.left != null) {
                    s.push(addQueue(node.left,q1));
                }


            }


        }


        return list;

    }


    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n5 = new TreeNode(5);

        n1.left = n2;
        n2.right = n5;
        n1.right = n3;

        for (String s : binaryTreePaths(n1)) {
            System.out.println(s);
        }
        ;


    }


    private static Deque<TreeNode> addQueue(TreeNode node, Deque<TreeNode> q1) {
        Deque<TreeNode> newQ = new LinkedList<>(q1);
        newQ.add(node);
        return newQ;
    }

    private static String constructString(Deque<TreeNode> q) {
        StringBuffer buff = new StringBuffer();
        while (q.size() > 0) {
            TreeNode poll = q.poll();
            buff.append(poll.val);
            if (q.size() > 0) {
                buff.append("->");
            }
        }
        return buff.toString();
    }


}
