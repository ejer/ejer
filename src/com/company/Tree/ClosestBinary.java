package com.company.Tree;

/**
 * Created by federicobrubacher on 9/3/15.
 */
public class ClosestBinary {

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public int closestValue(TreeNode root, double target) {

        int targetInt = (int) Math.round(target);

        while (root != null) {

            int rootAbs = Math.abs(root.val - targetInt);

            if (targetInt == root.val) {
                return root.val;
            }

            if (root.left == null && root.right == null) {
                return root.val;
            }

            if (targetInt < root.val) {

                if (root.left == null) {
                    return root.val;
                }

                if  (targetInt <= root.left.val) {
                    root = root.left;
                    continue;
                } else {
                    if ( rootAbs <= Math.abs(root.left.val - targetInt) ) {
                        return root.val;
                    } else {
                        root = root.left;
                    }
                }
            } else {

                if (root.right == null) {
                    return root.val;
                }


                if  (targetInt >= root.right.val) {
                    root = root.right;
                    continue;
                } else {
                    if ( rootAbs <= Math.abs(root.right.val - targetInt) ) {
                        return root.val;
                    } else {
                        root = root.right;
                    }
                }

            }





        }

        return 0;

    }


    public static void main(String[] args) {
// [5,3,6,2,4,null,null,1], 2.857143
        ClosestBinary c = new ClosestBinary();
        TreeNode t5 = new TreeNode(5);
        TreeNode t3 = new TreeNode(3);
        TreeNode t6 = new TreeNode(6);
        TreeNode t2 = new TreeNode(2);
        TreeNode t4 = new TreeNode(4);
        TreeNode t1 = new TreeNode(1);

        t5.left = t3;
        t5.right = t6;
        t3.left = t2;
        t3.right = t4;
        t6.left = t1;
        int closestValue = c.closestValue(t1, 2.857143);
        System.out.print(closestValue);

    }

}
