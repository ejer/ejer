package com.company.binary;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 8/10/15.
 */
public class medianFind {


    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }

    public static void main(String[] args) {

        PriorityQueue<Integer> high = new PriorityQueue<>(100, Collections.reverseOrder());
        PriorityQueue<Integer> low = new PriorityQueue<>(100);

        String data = "10\n1\n2\n3\n4\n5\n\6\n7\n8\n9\n10\n";
        System.setIn(new ByteArrayInputStream(data.getBytes()));

        Input2 in = new Input2();
        double median;

        int i1 = in.nextInt();


        for (int i = 0; i < i1; i++) {

            int mothers_gift = in.nextInt();
            if (high.isEmpty()) {
                high.add(mothers_gift);
            } else {
                if (mothers_gift <= high.peek()) {
                    high.add(mothers_gift);
                } else {
                    low.add(mothers_gift);
                }
            }

            mantainInvariant(high, low);


            // mantain invariant


            int diff;

            diff = low.size() - high.size();

            if (diff == 0) {
                median = (low.peek() + high.peek()) / 2.0;

            } else if (diff > 0) {
                median = low.peek();
            } else {
                median = high.peek();
            }

            System.out.println(median);

        }
    }

    private static void mantainInvariant(PriorityQueue<Integer> high, PriorityQueue<Integer> low) {
        int diff = low.size() - high.size();

        if (diff >= 1) {
            while (low.size() - high.size()  >= 0) {
                high.add(low.poll());
            }

        } else if (diff <= -1){
            while (low.size() - high.size() < 0) {
                low.add(high.poll());
            }
        }
    }
}
