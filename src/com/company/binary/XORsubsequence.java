package com.company.binary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicobrubacher on 8/9/15.
 */
public class XORsubsequence {



    static List<List<Integer>> solutions = new ArrayList<>();
    public static void createCombinations(int[] arr, int i, ArrayList<Integer> comb) {

        if (i == arr.length) {
//            System.out.print("Sol :");
//            for (int m : comb) {
//                System.out.print(m + " ");
            if (comb.size() > 0) {
                solutions.add(comb);
            }
//            }
            return;
        }

        ArrayList<Integer> copyComb = new ArrayList<>(comb);
        comb.add(arr[i]);
        createCombinations(arr, i + 1, comb);
        createCombinations(arr, i + 1, copyComb);




    }

    public static void createCombinationsClose(int[] arr) {


        for (int i = 0; i < arr.length; i++) {

            for (int j = i; j < arr.length; j++) {

                List<Integer> comb = new ArrayList<>();
                for (int k = i; k <= j; k++) {
                    comb.add(arr[k]);
                }

                solutions.add(comb);
            }
        }

    }


    public static void main(String[] args) {

        int[] nbr = { 4, 5, 7, 7};

        createCombinationsClose(nbr);

        int result = 0;
        for (List<Integer> solution : solutions) {
            int curr = 0;
            for (int sol : solution)  {
               curr ^= sol;
            }

            result ^= curr;
        }


        System.out.print(result);

    }

}
