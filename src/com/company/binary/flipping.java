package com.company.binary;

import java.util.Stack;

/**
 * Created by federicobrubacher on 8/8/15.
 */
public class flipping {

    public static void main(String[] args) {

       /* n = ((N)?floor( log10(N)/log10(2) ) + 1:0); //calculate number of digits in advance floor(log2(N)) + 1
        vector <int> bin(n);
        i = n-1;
        while(N!=0) {
            bin[i]=N%2;
            N/=2;
            i--;
        }*/

        Stack<Integer> bin = new Stack<Integer>();
        int number = 5;
        int inverse = 0;
        int digits = 0;
        while (number > 1) {
            bin.add(1);
            number /= 2;
        }
        // ^ is not power but XOR
        int sum = 0;
        for (int i = bin.size(); i > 0; i--) {
            Integer pop = bin.pop();
            if (pop==0) {
                sum += Math.pow(2,i-1);
            }
        }
        
        System.out.print(sum);




    }
}
