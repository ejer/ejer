package com.company.binary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by federicobrubacher on 8/16/15.
 */
public class PrintPathsSummingToN {

    /* first part is to check the paths starting from root */
    public static class Node<T> {
        T value;
        Node<T> left;
        Node<T> right;

        public Node(T i) {
            this.value = value;
        }

        public Node() {

        }
    }


    public static void treeT() {
        Queue<Integer>  q = new LinkedList<>();
        q.offer(3);
        q.poll();
    }

    public static void print(List<Node<Integer>> nodes) {

        for(Node node : nodes) {
            System.out.print(node.value + " ");
        }
    }

    public static void sumUntilNode(Node<Integer> node, int currentSum, int targetSum, List<Node<Integer>> nodes) {
        nodes.add(node);
        if (targetSum == currentSum + node.value) {
            print(nodes);
        } else {
            if (node.left != null) {
                List<Node<Integer>> nodeCopy = new ArrayList<>(nodes);
                sumUntilNode(node.left, currentSum + node.value, targetSum, nodeCopy);
            }
             if (node.right != null) {
                List<Node<Integer>> nodeCopy = new ArrayList<>(nodes);
                sumUntilNode(node.right, currentSum + node.value, targetSum, nodeCopy);
            }
        }
    }

    public static void main(String[] args) {
        Node<Integer> root = new Node<>();
        root.value = 3;
        root.left = new Node<Integer>(5);
        root.left.value  = 5;
        root.right = new Node<Integer>(1);
        root.right.value = 1;
        sumUntilNode(root, 0, 8, new ArrayList<Node<Integer>>());
    }


}
