//package com.company.graph2;
//
///**
// * Created by federicobrubacher on 1/13/15.
// */
//
///* There are T wizards in the kingdom and they have the ability to transform the skill set of a person into another skill set.
// Each of the these wizards has two list of skill sets associated with them, A and B.
//  He can only transform the skill set of person whose initial skill set lies in list A and that final skill set will be an element of list B.
//  That is, if A=[2,3,6] and B=[1,2] then following transformation can be done by that trainer. */
//
//public class TrainingTheArmy {
// int MAXA = 400005;
// int MAXV = 1005;
//
// int A,V,source,dest,id,oo = 1000000001;
// int cap[] = new int[MAXA];
//
// int flow[] = new int[MAXA];
// int ady[] = new int[MAXA];
// int next[] = new int[MAXA];
// int last[] = new int[MAXV];
// int now[] = new int[MAXA];
// int level[] = new int[MAXV];
// int Q[] = new int[MAXV];
//
//
// public static void add(int u ,int v, int c) {
//
// }
//
// Boolean bfs(int source2, int dest) {
//  int ini, end, u , v;
//  for (int i=0; i<level.length; i++) level[i] = -1;
//  ini = end = level[source2] = 0;
//  Q[end++] = source2;
//  while (ini<end && level[dest]==-1) {
//   u = Q[ini++];
//  }
//
// }
//}
//
//
//
///*
//This problem can be solve using max flow algorithms.
//The network can be built into the following way. A source and a sink. N nodes, one for each class, and T others nodes, one for each trainer.
//One edge with capacity 1 is added between each one of the N nodes toward the sink. One edge with capacity Ci is added between the source toward each one of the N nodes.
//Ci is how many soldiers belongs to that class at the beginning. Now let's model the process of changing soldier's classes.
//Each trainer is able to take a set of soldiers belonging to some classes, that he can train, but at most one per class,
// then each soldier will get the train and change is class (No necessary will change his class). This can be model this by way.
// Let's add one edge with capacity 1 between each class toward all the trainers who are able to train this class. Another edge with capacity 1,
// is needed to be added, between each trainer and all the classes he can convert soldiers to.
//The max flow of this network is the solution of the problem.
// */
