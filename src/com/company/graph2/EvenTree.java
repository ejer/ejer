package com.company.graph2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Created by federicobrubacher on 1/28/15.
 */
public class EvenTree {
    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }

    public static void main(String[] args) throws IOException {
        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/edgetree/1.txt")));
        Input2 in2 = new Input2();
        int nbrVertices = in3.nextInt();
        int nbrEdge = in3.nextInt();
        int edgesToRemove = evenTree(nbrVertices, nbrEdge, in3);
        System.out.println(edgesToRemove);

    }

    private static int evenTree(int nbrVertices, int nbrEdge, Input in3) throws IOException {
        // creating adjancy lists
        Map<Integer, List<Integer>> graph = new HashMap<Integer, List<Integer>>(nbrVertices);
        int root = 0;
        for (int i = 0; i < nbrEdge; i++) {
            int v1 = in3.nextInt();
            int v2 = in3.nextInt();
            if (i == 0) {
                root = v2;
            }
            createEdge(graph, v1, v2 );
        }

        // el algoritmo sería :
        // empiezo por el root, hago dfs, en cada nodo voy chequando si es divisible entre dos,
        // si no lo es y no es 0 , tiro un falso que se up-propagates e incluyo al root
        // en ejemplo 1  bfs ( 2 , bfs ( 7, 5), bfs (7), bfs(5) ) si esj

        dfs(root, graph);

        return edgesToPrune-1;
    }

    public static ArrayList<Integer> getValue(int key, Map<Integer, List<Integer>> graph) {
        if ( graph.containsKey(key) ) {
            return (ArrayList) graph.get(key);
        } else {
            return new ArrayList<Integer>();
        }
    }

    static int edgesToPrune = 0;
    private static int dfs(int root, Map<Integer, List<Integer>> graph) {
        List<Integer> rootElems = getValue(root, graph);
        int[] nbrSubs = new int[rootElems.size()];
        for (int i = 0; i < rootElems.size(); i++) {
            nbrSubs[i] =  dfs(rootElems.get(i), graph);
        }
        int totalSubVertex = 0;
        for (int nbrSub : nbrSubs) {
            totalSubVertex += nbrSub;
        }
        // meaning that we need an odd nbr of subvertices (plus this one) and then it should be even
        if (totalSubVertex % 2 != 0) {
            edgesToPrune += 1;
            totalSubVertex = 0; // reset the count of totalsubvertexs
            return totalSubVertex;
        }
        return totalSubVertex + 1;
    }

    private static void createEdge(Map<Integer, List<Integer>> graph, int v1, int v2) {
        if (graph.containsKey(v2)) {
            List<Integer> elems = graph.get(v2);
            elems.add(v1);
            graph.put(v2, elems);
        } else {
            List<Integer> elems = new ArrayList<Integer>();
            elems.add(v1);
            graph.put(v2, elems);
        }
    }
}
