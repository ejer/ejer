package com.company.hashMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by federicobrubacher on 8/21/15.
 */
public class HappyNumber {
    public boolean isHappy(int n) {
        Map<String,Integer> numberToSquares = new HashMap<>();

        for (int i = 0; i < 10; i++) {
            String num = String.valueOf(i);
            numberToSquares.put(num, (int) Math.pow(i, 2));
        }

        while (true) {
            String num1 = String.valueOf(n);
            char[] num = num1.toCharArray();
            int sum = 0;
            for(char c : num) {
                String s = String.valueOf(c);
                sum+=numberToSquares.get(s);
            }
            if (sum == 1) {
                return true;
            } else {
                n = sum;
            }
        }
    }

    public static void main(String[] args) {

        HappyNumber h = new HappyNumber();
        boolean happy = h.isHappy(1);
        System.out.print(happy);
    }
}
