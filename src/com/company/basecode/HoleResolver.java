package com.company.basecode;

import java.io.*;
import java.util.*;

/**
 * Created by federicobrubacher on 1/19/15.
 */
public class HoleResolver {

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static int[] dx = {0, 0, 1, -1};
    static int[] dy = {-1, 1, 0, 0};

    static char holeResolver(int toCompare, int curr, char currResult) {
        if ( curr > toCompare && currResult == 'X' ) {
            return 'X';
        } else {
            return Character.forDigit(curr, 10);
        }
    }

    public static void printChars(Input input) throws IOException {
        Integer _a = input.nextInt();

        if (_a == 1) {
            System.out.println(input.next());
        } else {
            // read

            char[][] grid = new char[_a][_a];
            for (int i = 0; i < _a; i++) {
                String line = input.next();
                char[] linesChar = line.toCharArray();
                for (int j = 0; j < _a; j++) {
                    grid[i][j] = linesChar[j];
                }
            }

            // process ( TODO: change me to process while reading)

            System.out.println(new String(grid[0]));
            for (int i = 1; i < _a - 1; i++) {
                char[] copyLinesChar = grid[i];
                for (int j = 1; j < _a - 1; j++) {
                    char result = 'X';
                    for (int d = 0; d < 4; d++) {
                        result = holeResolver(Character.getNumericValue(grid[i + dx[d]][j + dy[d]]),
                                Character.getNumericValue(grid[i][j]), result);
                    }
                    copyLinesChar[j] = result;
                }
                System.out.println(copyLinesChar);
            }
            System.out.println(new String(grid[_a - 1]));
        }
    }

    public static void main(String[] args) throws IOException {

        String loco = "4 \n"
        + "1112 \n"
        + "1912 \n"
        + "1892 \n"
        + "1234 \n";
        Input in  = new Input(loco);

        Input2 in2 = new Input2();

        System.out.println(in.nextInt());
        printChars(in);

    }
}
