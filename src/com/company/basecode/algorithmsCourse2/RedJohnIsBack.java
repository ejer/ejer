package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/19/15.
 */
public class RedJohnIsBack {



    public static void main(String[] args) {

        int i = calcBricks(values, 0, 7);
        System.out.print(i);


    }


    static int[] values = { 4, 1};

    public static int calcBricks(int[] brickvalues, int brickId, int bricksUsed) {
        /* la idea es que haya X * 4 + Y = N */

        if (bricksUsed == 0) {
            return 1;
        }

        if (bricksUsed < 0) {
            return 0;
        }

        if (brickId >= brickvalues.length && bricksUsed > 0) {
            return 0;
        }

        return calcBricks(brickvalues, brickId+1, bricksUsed) +
                calcBricks(brickvalues, brickId, bricksUsed-brickvalues[brickId]);


    }


}
