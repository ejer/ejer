package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/19/15.
 */
public class EightQueen {

    /* Backtracking :
    1) start in the leftmost coll

    2) If all the queens are placed return true

    3) Try all rows in the current coll , do the following for every tried row.

        a) If the queen can be placed safely in this row mark this [row, coll] as part of the solution
        and recursively check if placing queen here leads to a solution

        b) If placing queen in [row, coll] leads to a solution then return true
        c) If placing queen doesn't lead to a solution then unmark this [row , coll] (Backtrack) and go to step one to try other rows
    4) If all rows have been tried and nothing worked then return false to trigger backtracking


    DFS

    more on a dfs style, start on one of the colls place it there and see if it fits, then recurse adding one to the coll
     */



    public static int[][] board = {{ 0, 0, 0, 0}, {0, 0, 0, 0}, {0,0,0,0}, {0,0,0,0}};

    public static void main(String[] args) {

        solve(board, 0);

        for (int[] row : board) {

            for (int col : row) {
                System.out.format("%5d", col);
            }
            System.out.println();
        }

    }

    private static boolean solve(int[][] board, int coll) {
        if (coll >= N)
            return  true;

        for (int i = 0; i < N; i++) {

            if (checkValid(board, i, coll)) {
                board[i][coll] = 1;
                if (solve(board, coll+1) == true) {
                    return true;
                }
                board[i][coll] = 0;
            }
        }
        return  false;
    }


    public static final int nbrCollsRows = 4;
    public static final int N = 4;


    private static boolean checkValid(int[][] board, int row, int col) {

        int i, j;

    /* Check this row on left side */
        for (i = 0; i < col; i++)
        {
            if (board[row][i] == 1)
                return false;
        }

    /* Check upper diagonal on left side */
        for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
        {
            if (board[i][j] == 1)
                return false;
        }

    /* Check lower diagonal on left side */
        for (i = row, j = col; j >= 0 && i < nbrCollsRows; i++, j--)
        {
            if (board[i][j] == 1)
                return false;
        }

        return true;
    }



}
