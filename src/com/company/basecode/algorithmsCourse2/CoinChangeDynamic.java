package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/19/15.
 */
public class CoinChangeDynamic {

    public static int count(int S[], int m, int n) {

        int[][] table = new int[n+1][m];

        /* tengo que inicializar todos en 1

            one denomination available
         */

        for (int i = 0; i < m; i++) {
            table[0][i] = 1;
        }

        for (int i = 1; i < n + 1; i++) {
            for (int j = 0; j < m; j++) {

                int x  = (i-S[j] >= 0)? table[i-S[j]][j]:0;

                int y = (j-1>=0)? table[i][j-1]:0;
                table[i][j] = x + y;
            }
        }

        return table[n][m-1];


    }


    public static void main(String[] args)  {

        int[] arr = { 2, 5, 3, 6};
        int m = arr.length;

        int n = 10;

        System.out.print(count(arr, m,n));
    }
}
