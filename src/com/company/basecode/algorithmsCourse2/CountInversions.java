package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/15/15.
 */
public class CountInversions {

    public static int[] test = { 1, 2, 3, 7, 8, 5, 9};

    public static int[] output;

    static long nbrOfInversons = 0;


    private static void copyArray(int[] input, int beg, int end ,int[] output) {
        for (int i = beg; i < end; i++) {
            input[i] = output[i];
        }
    }

    public static int sort(int start, int end) {

        if (end - start < 2) {
            return 0;
        }

        int med = (start+end)/2;
        sort(start, med);
        sort(med+1, end);

        merge(start, med, end, output);
        copyArray(test, start, end, output);
        return 0;

    }
    public static void printArr(int[] args) {
        for (int arg : args) {
            System.out.print(arg);
        }
    }

    private static void merge(int start, int med, int end, int[] output) {
        /* la idea es que start-med y med+1,end ya estan sorted pero les flata mergearse
            para mergear
            esta es la version wikipedia
         */
        int i0 = start;
        int i1 = med;
        for (int k = start; k <= end; k++) {
            if (i0 < med && (i1 >= end || test[i0] < test[i1])) {
                output[k] = test[i0];
                i0++;
            } else {
                output[k] = test[i1];
                i1++;
            }
        }
    }








    public static void main(String[] args) {

        output = new int[test.length];
        sort(0, test.length-1);

        printArr(output);

    }


}
