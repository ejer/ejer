package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/21/15.
 */
public class SubstringDiff {


    public static int subStringDiff( int i, int j, int Li, int Lj) {

        /* algo :

        if (nbrEq(s1, s2) <= S) {
            return L
         }

         Sino la idea es que veo cual es mejor
         sacando uno de i o uno de j

         Math.max(

         subStringDiff ( s1.subString(i+1, L), s2.subString(j, L), L-1, i+1, j, S) ,

         subStringDiff ( s1.subString(i,L-1), s2.subString(j+1, L), L-1, i, j-1, S) ,

         */


        String sub1 = s1.substring(i, Li);
        String sub2 = s2.substring(j, Lj);

        if (nbrEq(sub1,sub2) <= S) {
            return sub1.length();
        }


        return Math.max(subStringDiff(i+1, j, Li, Lj-1), subStringDiff(i,j+1,Li-1,Lj));


    }

    private static int nbrEq(String s1, String s2) {

        int mismatches = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) != s2.charAt(i))
                mismatches++;
        }
        return mismatches;
    }

    public static String s1 = "tabriz";
    public static String s2 = "torino";
    public static int S = 2;
    public static void main(String[] args) {





        int result  = subStringDiff(0,0,s1.length(), s2.length());
        System.out.print(result);



    }

}
