package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/19/15.
 */
public class CoinChange {

    public static int[] coins = {2, 5, 3, 6};

    public static int sum = 0;

    public static void main(String[] args) {

        int result = includeCoin(coins, 10, coins.length-1);
        System.out.print(result);
        System.out.print(sum);
    }



    public static int includeCoin(int[] coinsValues, int currSum, int coinNbr) {

        /* la recursión es :


            nr of solutions including m , nbr of solutions excluding mtr

         */
        if (currSum == 0) {
            sum += 1;
            return 1;
        }

        if ( currSum < 0)
            return 0;

        if (coinNbr <= 0 && currSum >= 1)
            return 0;



        return includeCoin(coinsValues, currSum-coins[coinNbr], coinNbr)
                + includeCoin(coinsValues, currSum, coinNbr-1);



    }
}
