package com.company.basecode.algorithmsCourse2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicobrubacher on 7/18/15.
 */
public class QuickSort {
    public static int[] test = {10 , 8 , 3, 4, 2 };
    public static int[] result;






    public static void printArr(List<Integer> args) {
        for (int arg : args) {
            System.out.print(arg);
        }
    }


    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<Integer>();
        integers.add(10);
        integers.add(8);
        integers.add(3);
        integers.add(4);
        integers.add(2);


        List<Integer> integers1 = quickSort(integers);

        printArr(integers1);
    }

    private static List<Integer> quickSort(ArrayList<Integer> input) {

        int size = input.size();
        if (size <= 1) {
            return input;
        } else {
            Integer pivot = input.get(0);
            return partition(pivot, input.subList(1, size));
        }

    }


    public static int[] A;


    public static void swap(int a, int b) {
        int temp = A[a];
        A[a] = A[b];
        A[b] = temp;
    }

    private static List<Integer> partition(Integer pivot, List<Integer> input) {
        int size = input.size();
        ArrayList<Integer> lesser = new ArrayList<Integer>(size);
        ArrayList<Integer> greater = new ArrayList<Integer>(size);

        for (int i = 0; i < size; i++) {
            Integer cur = input.get(i);
            if (cur <= pivot) {
                lesser.add(cur);
            } else {
                greater.add(cur);
            }

        }

        List<Integer> lesserSorted = quickSort(lesser);
        List<Integer> greaterSorted = quickSort(greater);
        lesserSorted.add(pivot);
        lesserSorted.addAll(greaterSorted);
        return lesserSorted;
    }
}
