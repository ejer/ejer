package com.company.basecode.algorithmsCourse2;

/**
 * Created by federicobrubacher on 7/20/15.
 */
public class RedJohnIsBackDynamic {


    public static void main(String[] args) {



        int m = 7;
        int[] table = new int[m+1];
        table[0] = 1;
        table[1] = 1;
        table[2] = 1;
        table[3] = 1;
        for (int i = 4; i <= m; i++) {
            table[i] = table[i-1] + table[i-4];
        }

        System.out.println(table[m]);


    }
}
