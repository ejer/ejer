package com.company;

/**
 * Created by federicobrubacher on 11/8/14.
 */
public class LCS2 {
    public static final String fede = "hola xarola";
    public static final String fede2 = "carha";



    public static int LCS2(int agg, char[] word1, char[] word2, int m, int n){
        if (m == 0 || n == 0) {
            return agg;
        }
        if (word1[m] == word2[n])
        {
            return LCS2(agg+1, word1, word2, m-1, n-1);
        }
        else {
            return Math.max(LCS2(agg, word1, word2, m-1, n), LCS2(agg, word1, word2, m, n-1));
        }
    }

    public static void main(String[] args) {
        System.out.println("RESULTADO :" + LCS2(0, fede.toCharArray(), fede2.toCharArray(), fede.length()-1, fede2.length()-1));

    }
}
