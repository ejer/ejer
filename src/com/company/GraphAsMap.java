package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by federicobrubacher on 12/23/14.
 */
public  class GraphAsMap<T> {
     Map<T, List<T>> graphList = new HashMap<T, List<T>>();


     public void add(T node, List<T> vertices) {
          graphList.put(node, vertices);
     }

     // Backtracking all algorithms
     public List<T> find_path(T start, T end, List<T> path) {
          path.add(start);
          if (start == end) {
               return path;
          }
          if (!(graphList.containsKey(start))) {
              return null;
          }
          for (T node : graphList.keySet() )  {
              if (!(path.contains(node))){

                   List newPath = find_path(node, end, path);
                   if (newPath != null) return newPath;
               }
          }
          return null;
     }


     public List<T> find_shortest_path(T start, T end, List<T> path) {
          path.add(start);
          if (start == end) {
               return path;
          }
          if (!(graphList.containsKey(start))) {
              return null;
          }
          List<T> shortest = null;
          for (T node : graphList.keySet()) {
               if (!(path.contains(node))) {
                    List<T> newPath = find_shortest_path(node, end, path);
                    if (newPath != null)
                    {
                         if (shortest == null || newPath.size() < shortest.size()) {
                              shortest = newPath;
                         }
                    }
               }
          }
          return  shortest;
     }

     public static void main(String[] args) {
          GraphAsMap<Integer> g = new GraphAsMap<Integer>();
//          List<Integer> list3 = new ArrayList<Integer>();
//          list3.add(5);
//          list3.add(6);
//           List<Integer> list6 = new ArrayList<Integer>();
//          list3.add(7);
//           List<Integer> list7 = new ArrayList<Integer>();
//          list3.add(8);
//          g.add(3 , list3);
//          g.add(6, list6);
//          g.add(7, list7);
//          List<Integer> list7 = new ArrayList<Integer>();
//          find_path(3, 8, );
     }
}
