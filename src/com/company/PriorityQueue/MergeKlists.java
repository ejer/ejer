package com.company.PriorityQueue;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by federicobrubacher on 8/30/15.
 */


public class MergeKlists {

    public static class ListNode  {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }


    }

    public class ListNodeComparator implements Comparator<ListNode> {
        @Override
        public int compare(ListNode o, ListNode o1) {
            if (o.val == o1.val) {
                return 0;
            }
            int i = o.val > o1.val ? 1 : -1;
            return i;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {

        PriorityQueue<ListNode> pq = new PriorityQueue<ListNode>(lists.length, new ListNodeComparator());


        for (int i = 0; i < lists.length; i++) {

            ListNode node = lists[i];
            if (node != null) {
                pq.offer(node);

            }
            while (node.next != null) {
                node = node.next;
                pq.offer(node);
            }

        }

        ListNode first = pq.peek();
        ListNode curr = null;

        while (pq.peek() != null) {
            curr = pq.poll();
            curr.next = pq.peek();
        }


        return first;


    }


    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l5 = new ListNode(5);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(3);
        l1.next = l5;
        l2.next = l3;
        ListNode[] nodes = {l1, l2};
        MergeKlists mergeKlists = new MergeKlists();
        mergeKlists.mergeKLists(nodes);

    }


}
