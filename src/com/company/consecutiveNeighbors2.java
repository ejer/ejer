package com.company;

/**
 * Created by federicobrubacher on 12/22/14.
 */
// A square matrix of size n^2 and random entries from   0 ... n^2, find the longest sequence of consecutive neighbours (i.e. top, left, bottom, right entries).
//
public class consecutiveNeighbors2 {

    static int[][] matrixNumbers = new int[][] { {5, 2}, {4, 9}};
    static int l = matrixNumbers.length;

    public static int calculateLongestNeighbors(int acc, int i, int j, int prev) {
       if (i >= l || j >=l) {
            return acc;
        }
        if (i < 0 || j < 0) {
            return acc;
        }
        int current = matrixNumbers[i][j];

//        int dx = { -1, 0 , 1};
        System.out.println(current);
        if (current == prev + 1 || prev == 0) {
            acc = acc +1;
            int max1 = Math.max(
                    calculateLongestNeighbors(acc, i + 1, j, current),
                    calculateLongestNeighbors(acc, i, j+1, current)
            );
            int max2 = Math.max(
                    calculateLongestNeighbors(acc, i-1, j, current),
                    calculateLongestNeighbors(acc, i, j-1, current)
            );
            return Math.max(max1, max2);
        } else {
            System.out.println(acc);
            return acc;
        }

    }

    public static int calculate(){
        int maxResult = 0;
        for(int x = 0; x < l; x++) {
            for(int z = 0; z < l; z++) {
                int localResult = calculateLongestNeighbors(0, x, z, 0);
                if (localResult > maxResult) maxResult = localResult;
            }
        }
        return maxResult;
    }

    public static void main(String[] args) {
       System.out.println("RESULTADO FINAL"  + calculate());

    }
}
