package com.company.LinkedList;

/**
 * Created by federicobrubacher on 11/30/15.
 */
public class removeDupes {

    public ListNode remove(ListNode head) {
        ListNode curr = head;
        ListNode next = curr.next;

        if (head == null || next == null) return head;
        while(next != null) {
            if(curr.val == next.val) {
                next = next.next;
                if (next != null) continue;
            }
            curr.next = next;
            curr = next;
            if (curr != null) next = curr.next;
        }
        return head;
    }

    public static void main(String[] args) {

        removeDupes i = new removeDupes();
        ListNode l = new ListNode(1);
        ListNode l1 = new ListNode(2);
        ListNode l2 = new ListNode(3);
        ListNode l3 = new ListNode(3);
        ListNode l4 = new ListNode(3);
        ListNode la = new ListNode(1);
        ListNode lb = new ListNode(1);
        ListNode lc = new ListNode(1);

        l.next = la;
        la.next = lb;
        lb.next = lc;
        lc.next = l1;
        l1.next = l2;
        l2.next = l3;
        l3.next = l4;

        ListNode listNode = i.remove(l);

        System.out.print("hello");
    }
}
