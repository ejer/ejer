package com.company.LinkedList;

/**
 * Created by federicobrubacher on 11/30/15.
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}
