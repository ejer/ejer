package com.company.LinkedList;

/**
 * Created by federicobrubacher on 8/22/15.
 */
public class Partition {


    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }


    public ListNode partition(ListNode head, int x) {


        int afterCount = 0;
        int beforeCount = 0;
        ListNode after_head = null;
        ListNode before_head = null;
        ListNode before = null;
        ListNode after = null;

        while ( true )  {

            if (head == null) {
                break;
            }
            if (head.val < x) {
                if (beforeCount == 0) {
                    before_head = head;
                }
                before = head;
                before = before.next;
                head = head.next;
                beforeCount++;
            } else {
                if (afterCount == 0) {
                    after_head = head;
                }
                after = head;
                after = after.next;
                head = head.next;
                afterCount++;
            }

        }


        before = after_head;

        if ( before_head == null) {
            return after_head;
        } else {
            return before_head;
        }

    }

    public static void main(String[] args) {

        Partition i = new Partition();
        ListNode l = new ListNode(1);
        ListNode l1 = new ListNode(2);
//        ListNode l2 = new ListNode(3);
        l1.next = l;
//        l1.next = l2;
        ListNode listNode = i.partition(l1,1);

        System.out.print("hello");
    }

}
