package com.company.LinkedList;

public class Invert {


    /* 1 -> 2 -> 3 -> null
    3 -> 2 -> 1 -> null

    */

    public ListNode rev(ListNode curr, ListNode prev) {

        if (curr == null) {
            return prev;
        }
        ListNode next = curr.next;
        curr.next = prev;
        return rev(next, curr);

    }

    public ListNode reverseList(ListNode head) {
        if (head == null) {
            return head;
        }
        return rev(head.next, head);

    }

    public ListNode reverseList2(ListNode head) {
        if (head == null) return head;
        ListNode curr = head;
        ListNode next = curr.next;
        // A -> B -> C  ... A <- B <- C
        while(next != null) {
            ListNode tmp = next.next;
            next.next = curr;
            curr = next;
            next = tmp;
        }

        return curr;

    }

    public static void main(String[] args) {

        Invert i = new Invert();
        ListNode l = new ListNode(1);
        ListNode l1 = new ListNode(2);
        ListNode l2 = new ListNode(3);
        l.next = l1;
        l1.next = l2;

        ListNode listNode = i.reverseList2(l);

        System.out.print("hello");
    }

}
