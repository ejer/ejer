package com.company.LinkedList;

/**
 * Created by federicobrubacher on 12/5/15.
 */
public class Sum2Lists {

    public boolean carry = false;


    public ListNode addValue(ListNode l1, ListNode l2) {
        int l3Val = l1.val+l2.val;
        if (carry == true) {
            l3Val+=1;
            carry = false;
        }
        if (l3Val/10>0) carry = true;
        return new ListNode(l3Val%10);
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) return l1;
        if (l2 == null) return l1;
        if (l1 == null) return l2;
        ListNode n1 = l1.next;
        ListNode n2 = l2.next;

        ListNode l3 = addValue(l1, l2);
        ListNode curr = l3;

        while (n1 != null && n2 != null) {

            curr.next = addValue(n1, n2);
            curr = curr.next;
            n1 = n1.next;
            n2 = n2.next;
        }

        while (n1 != null) {
            curr.next = addValue(new ListNode(0), n1);
            curr = curr.next;
            n1 = n1.next;
        }

        while (n2 != null) {
            curr.next = addValue(new ListNode(0), n2);
            curr = curr.next;
            n2 = n2.next;
        }

        if(carry) {
            curr.next = new ListNode(1);
        }

        return l3;
    }

    public static void main(String[] args) {
        ListNode listNode = new ListNode(5);
        ListNode listNode1 = new ListNode(5);
        Sum2Lists sum2Lists = new Sum2Lists();
        ListNode listNode2 = sum2Lists.addTwoNumbers(listNode, listNode1);
        System.out.print(listNode2.val);

    }
}
