package com.company.codeJamPractice;

import java.io.*;
import java.util.*;

/**
 * Created by federicobrubacher on 11/28/15.
 */
public class Bribe {

    public static PrintWriter out;
    public static Scanner in;
    public static String testCase = "a2";


    int free(int[] toFree, int toFreeId, int numPrisoners, List<Integer> freed) {

        if (toFree.length == freed.size()) return 0;

        for (int i = toFreeId; i < toFree.length; i++) {
            int curr = toFree[i];
            int suma = 0;

            if (freed.contains(curr)) continue;

            // bigger

            for (int j = curr+1; j <= numPrisoners; j++) {
                if (freed.contains(j)) {
                    break;
                }
                suma += 1;
            }

            //smaller

            for (int j = curr-1; j >= 0; j--) {
                if (freed.contains(j)) {
                    break;
                }
                suma += 1;
            }


            List<Integer> ls = new ArrayList<>(freed);
            ls.add(curr);

            return Math.min(
                    suma + free(toFree, 1, numPrisoners, ls),
                    free(toFree, i+1, numPrisoners, freed));
        }

        return -1;
    }

    public static void main(String[] args) throws FileNotFoundException {
//           out = new PrintWriter(new FileOutputStream(testCase  + ".out"));
//        in = new Scanner(new BufferedReader(new InputStreamReader(new FileInputStream(testCase + ".in"))));
//
//        int N = in.nextInt();
//        for (int i = 1; i <= N; ++i) {
//
//        }

        int[] toFree = {1, 3, 4, 9};
        Bribe b = new Bribe();
        int free = b.free(toFree, 1, 10, new ArrayList<Integer>());
        System.out.print(free);


    }


}
