package com.company.codeJamPractice;

import java.io.*;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 11/29/15.
 */
public class Bribe2 {
    public static int[][] answers;
    public static int[] pardoned;
    public static PrintWriter out;
    public static Scanner in;
    public static String testCase = "cl";

    public static void main(String[] args) throws FileNotFoundException {
        out = new PrintWriter(new FileOutputStream(testCase + ".out"));
        in = new Scanner(new BufferedReader(new InputStreamReader(new FileInputStream(testCase + ".in"))));

        int N = in.nextInt();
        for (int i = 0; i < N; i++) {
            int P = in.nextInt();
            int Q = in.nextInt();
            Bribe2 b = new Bribe2();
            answers = new int[Q+1][Q+1];
            pardoned = new int[Q+2];
            pardoned[0] = 0;
            pardoned[Q+1] = P+1;
            for (int j = 1; j <= Q; j++) {
                pardoned[j] = in.nextInt();
            }
            out.println("Case #" + (i+1) + ":" + " " + b.minCost(1,Q));
        }

        in.close();
        out.close();
    }

    public int minCost(int start, int finish) {
        if (finish - start < 0) {
            return 0;
        }

        if (answers[start][finish] != 0) {
            return answers[start][finish];
        }

        int min = Integer.MAX_VALUE;
        for (int i = start; i <= finish; i++) {
            min = Math.min(min,
                    pardoned[finish + 1] - pardoned[start - 1] - 2 +
                            minCost(start, i - 1) + minCost(i + 1, finish));


        }
        answers[start][finish] = min;
        return min;
    }


}
