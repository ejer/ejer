package com.company.june;

/**
 * Created by federicobrubacher on 6/13/15.
 */
public class BinaryTree {

    static int keys[] = {10, 12, 20};
    static int freq[] = {34, 8, 50};
    static int n = keys.length;
    public static int[][] A = new int[n+1][n+1];

    public static void main(String[] args) {

        for (int i = 0; i < n; i++) {
            A[i][i] = freq[i];
        }

        for (int s = 2; s <= n; s++) {
            for (int i = 0; i <= s+1; i++) {
                A[i][i+s] = find_min(i, s);
            }
        }

        for (int[] rows : A) {
            for (int cols : rows) {
                System.out.format("%5d", cols);
            }
            System.out.println();
        }

        System.out.print(A[1][n]);
    }

    private static int find_min(int i, int s) {
        int min = Integer.MAX_VALUE;
        for (int r = i; r <= i+s; r++) {
            int pk = sum_freq(i, s);

            pk += A[i][r-1];
            pk += A[r+1][i+s];

            min = Math.min(min, pk);
        }
        return min;
    }

    private static int sum_freq(int i, int s) {
        int sum = 0;
        for (int j = i; j < i+s; j++) {
            sum += freq[j];
        }
        return sum;
    }

}
