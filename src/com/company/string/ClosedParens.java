package com.company.string;

import com.company.graph.Stack;

/**
 * Created by federicobrubacher on 8/28/15.
 */
public class ClosedParens {


    public static boolean helper(String s) {

        /* algo :

            {}()



            {(})


         */

        Stack<Integer> q = new Stack<>();


        if (s.length() == 0) {
            return true;
        }

        if (s.length() % 2 != 0) {
            return false;
        }

        for (int j = 0; j < s.length(); j++) {

            char c = s.charAt(j);

            if (c == '{' || c == '(' || c == '[') {
                q.push((int) c);
            } else {
                Integer pop = q.pop();
                if (Math.abs(pop - (int)c) > 3) {
                    return false;
                }

            }

        }

        if (q.size() == 0) {
            return true;
        } else {
            return false;

        }


    }


    public static void main(String[] args) {

        String test = "[({[]})";
        String test2 = ")";

        System.out.print(helper(test));




    }


}
