package com.company.search;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by federicobrubacher on 8/27/15.
 */
public class ContainsDict {


    static Set<String> dict = new HashSet<>();

    public static void main(String[] args) {
        String s = "leetcode";
        dict.add("lxet");
        dict.add("code");
//        System.out.print(findWord(s, 0));
        System.out.print(findWordDynamic(s));

    }

    public static boolean findWordDynamic(String s) {

        boolean[] t = new boolean[s.length() + 1];

        t[0] = true;


        for (int i = 0; i < s.length(); i++) {


            for (String entry : dict) {

                int sum = entry.length() + i;

                if (sum > s.length()) {
                    continue;
                }

                if (t[i] && s.substring(i, sum).equals(entry)) {
                    t[sum] = true;
                }

            }

        }


        return t[s.length()];
    }

    private static boolean findWord(String s, int start) {


        if (start == s.length()) {
            return true;
        }

        for (String entry : dict) {

            int entryLength = entry.length();
            int proposedLength = start + entryLength;
            if (proposedLength > s.length()) {
                continue;
            }
            if (s.substring(start, proposedLength).equals(entry)) {
                return findWord(s, proposedLength);
            }

        }
        return false;
    }


}
