package com.company.graph2flows;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by federicobrubacher on 1/8/15.
 */

public class minstcut {
    static int V = 3;
    public static void mincut(int graph[][], int s, int t) {
        int r_graph[][] = new int[V][V];
        for (int u =0; u < V; u++){ // Initializes the residual graph with the weights of the original graph
            for (int v = 0; v < V; v++) {
                r_graph[u][v] = graph[u][v];
            }
        }
        int[] parent = new int[V]; // This a array filled by BFS to store path;
        int u;
        while(bfs(r_graph, s, t, parent)) {
            int path_flow = Integer.MAX_VALUE;
            for (int v=t; v!=s; v=parent[v] )
            {
                u = parent[v];
                path_flow = Math.min(path_flow, r_graph[u][v]);

                r_graph[u][v] -= path_flow;
                r_graph[v][u] += path_flow;
            }

        }

        Boolean[] visited = new Boolean[V];
        dfs(r_graph, s, visited);

        for (int i = 0; i < V; i++){
            for (int j=0; j<V; j++) {
                if (visited[i] && !visited[j] && graph[i][j] != 0) {
                    System.out.println(i + " - " + j);
                }
            }
        }
    }

    private static void dfs(int[][] r_graph, int s, Boolean[] visited) {
        visited[s] = true;
        for (int i=0; i<V; i++) {
            if (r_graph[s][i] != 0 && !visited[i]) {
                dfs(r_graph, i, visited);
            }
        }
    }

    private static boolean bfs(int[][] r_graph, int s, int t, int[] parent) {
       Boolean[] visited = new Boolean[V];
       for (int i =0;i<visited.length;i++) visited[i] = false;
        Queue<Integer> q = new PriorityQueue<Integer>();
        q.add(s);
        while (!q.isEmpty()) {
            int u = q.poll();
            for (int v=0; v<V; v++) {
                if(visited[v] == false && r_graph[u][v] > 0) {
                    q.add(v);
                    parent[v] = u;
                    visited[v] = true;
                }
            }

        }
        return (visited[t] == true);
    }


    public static void main(String[] args) {
        int graph[][] = new int[][] { {0, 16, 13, 0, 0, 0},
            {0, 0, 10, 12, 0, 0},
            {0, 4, 0, 0, 14, 0},
            {0, 0, 9, 0, 0, 20},
            {0, 0, 0, 7, 0, 4},
            {0, 0, 0, 0, 0, 0}
        };

        mincut(graph, 0, 5);


    }
}
