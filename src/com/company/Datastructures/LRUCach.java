package com.company.Datastructures;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by federicobrubacher on 9/4/15.
 */
public class LRUCach {

    LinkedList<Integer> ll;
    Map<Integer, Integer> map;


    public LRUCach(int capacity) {
        map = new HashMap<>(capacity);
        ll = new LinkedList<>();
    }

    public int get(int key) {

        Integer value = map.get(key);
        if (value == null) throw new NoSuchElementException();
        int index = ll.indexOf(key);
        ll.remove(index);
        ll.addFirst(key);
        return value;
    }

    public void set(int key, int value) {
        ll.addFirst(key);
        map.put(key, value);

    }

}
