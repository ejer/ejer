package com.company;

import java.io.*;
import java.util.*;

/**
 * Created by federicobrubacher on 11/23/14.
 */
class Node {
    List<Node> parents = new ArrayList<Node>();
    int value=0;

    public Boolean getVisitedBackBefore() {
        return visitedBackBefore;
    }

    public void setVisitedBackBefore(Boolean visitedBackBefore) {
        this.visitedBackBefore = visitedBackBefore;
    }

    public int getValuedVisited() {
        visitedBackBefore = true;
        return value;
    }

    Boolean visitedBackBefore = false;

    public Node(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


    public List<Node> getParents() {
        return parents;
    }

    public void addParent(Node parent) {
        this.parents.add(parent);
    }

    public boolean hasParent() {
        if (parents.isEmpty())
            return false;
        return true;
    }
}

public class treepruning {
    public static final long MINF = -1000000000000000000L;
    static int[][] m;
    static ArrayList<Integer> SumSubtree2 = new ArrayList<Integer>();
    static HashMap<Integer, ArrayList<Node>> map = new HashMap<Integer, ArrayList<Node>>();
    static Node[] nodes;
    static HashMap<Integer, Integer > map2 = new HashMap<Integer, Integer>();

    public static void dfs(Node root){
        if (!map.containsKey(root.getValue()))
        {
            if (!root.getVisitedBackBefore())
                SumSubtree2.add(root.getValue());
            map2.put(root.getValue(), 0);
            Node currentNode = root;
            amountOfChildren(currentNode, 0);
        } else {
            SumSubtree2.add(root.getValue());
            List<Node> children =  map.get(root.getValue());
            for(Node child : children) {
                dfs(child);
            }
        }
    }

    private static void amountOfChildren(Node currentNode, int weight) {
        if (map2.containsKey(currentNode.getValue())){
            int v  =  map2.get(currentNode.getValue());
            if (!currentNode.getVisitedBackBefore()){
                map2.put(currentNode.getValuedVisited(), v+weight);
            }
        } else {
            if (!currentNode.getVisitedBackBefore()) {
                map2.put(currentNode.getValuedVisited(), weight);
            }
        }
        weight = weight + 1;
        for (Node parent : currentNode.getParents()) {
            amountOfChildren(parent, weight);
        }

    }

    public static void treePruning(Input in, PrintWriter out)  throws  IOException {
        int n = in.nextInt();
        int J = in.nextInt();
        m = new int[n+1][J+1];
        long[] w = new long[n];
//        Random rnd = new Random();
        ArrayList<Integer>[] edges = new ArrayList[n];
        for (int i = 0; i < n; ++i) {
            w[i] = in.nextLong();
        }
        int root = 0;
        int nbrElem = 0;

        nodes = new Node[100];
        for (int i = 0; i < 100; i++){
            nodes[i] = new Node(i+1);
        }
        for (int i = 0; i < n - 1; ++i) {
            int u = in.nextInt();
            int v = in.nextInt();
            if (i==0) {
                root = u;
            }
            if (v == root) {
                root = u;
            }
            if (map.containsKey(u))
            {
                ArrayList<Node> children = map.get(u);
                Node new_node= nodes[v-1];
                new_node.addParent(nodes[u-1]);
                children.add(new_node);
                map.put(u, children);
                nbrElem++;
            } else {
                ArrayList<Node> children = new ArrayList<Node>();
                Node new_node= nodes[v-1];
                new_node.addParent(nodes[u-1]);
                children.add(new_node);
                map.put(u, children);
                nbrElem+=2;
            }
        }

//        dfs(nodes[root-1]);

        for (int nodeId : map.keySet()) {
            Node node = nodes[nodeId-1];
            if (!node.hasParent())
                dfs(node);

        }




        int curr_j = 0;
        int[] SumSubtree3  = new int[SumSubtree2.size()];
        int[] subTreeLength3  = new int[SumSubtree2.size()];

        for (int i=0; i < SumSubtree2.size(); i++) {
            Integer pos =  SumSubtree2.get(i);
            SumSubtree3[i] = (int) w[pos-1];
            subTreeLength3[i] = map2.get(pos);
        }

        for (int i=1; i <=  n; i++) {
            for (int j=curr_j; j< J; j++) {
                treepruning.m[i][j] = Math.max(treepruning.m[i-1][j] + SumSubtree3[i-1], treepruning.m[i][j]);
                treepruning.m[i + subTreeLength3[i-1]][j+1] = Math.max(treepruning.m[i + subTreeLength3[i-1]][j+1], treepruning.m[i-1][j]);
            }
        }
//        for (int[] rows : treepruning.m) {
//            for (int col : rows) {
//                System.out.format("%5d", col);
//            }
//            System.out.println();
//        }
//        System.out.println("RESULTADO :" + m[n][J]);
        System.out.println(m[n][J]);
    }


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws  IOException{
//        treePruning();
        PrintWriter out = new PrintWriter(System.out);
        String loco = "5 2\n" +
                "-25891504 475306172 -513406545 240245539 -884787656\n" +
                "2 1\n" +
                "3 2\n" +
                "4 3\n" +
                "5 1";
//        Input in = new Input(new BufferedReader(new InputStreamReader(System.in)));
        Input in  = new Input(loco);
        treePruning(in, out);
    }
}
