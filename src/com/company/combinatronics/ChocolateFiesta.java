package com.company.combinatronics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 1/29/15.
 */
public class ChocolateFiesta {
    // if number is even
    // 2^n = 2^n/2 * mod  * 2^n/2 * mod
    // if number is odd
    // 2(n-1)*2
    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static int nbrEx;

    public static void NbrOfSubsets(Input2 input) {

        nbrEx = input.nextInt();
        double nbrEx2 = (double) nbrEx;
    }

    public static void main(String[] args) throws IOException {

        Input    in = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/chocolate/1.txt")));
        int nbr = in.nextInt();
        int result = calculateNbr(nbr, in);
        System.out.println(result);
    }

    private static int calculateNbr(int i, Input in) throws IOException {

        List<Integer> odd = new ArrayList<Integer>();
        List<Integer> even = new ArrayList<Integer>();

        for (int j = 0; j < i; j++) {
            int nbr = in.nextInt();
            if ( nbr % 2 == 0) {
                even.add(nbr);
            } else {
                odd.add(nbr);
            }
        }

        // Algorithm :
        // 1) generate all subsets of even nbrs
        // 2) take a even number of odd numbers and append it to the subsets of odd numbers

        // 1) 2^n -1

         int nbr = calculateNbr(even.size())-1;

        // 2)  Los elmentos son : < 2,4,6,1>
        // {2}, {4}, {6}, {2, 4}, {2, 6}, {4, 6}, {2, 4, 6}.  ( si los elementos fueran <2,4,6,1,1>)
        //  => {2,1,1}, {4,1,1}, {6,1,1}, {2, 4,1,1}, {2, 6,1,1}, {4, 6,1,1}, {2, 4, 6,1,1} +  {1, 1}
        // <2,4,6,1,1,5,7>
        //  => combinaciones con <1,1> combinaciones con <1, 5> combinaciones con <1,5> <1,7> <1,7> , <5,7> +  {1, 1}
        // combinaciones de 4 tomadas de a dos + combinaciones de 4 tomads de a 4
        // for 2 the combinations are : 511

        for (int j = 1; j <= odd.size(); j++) {
            if (j % 2 == 0) {

                int mult = j /2;
                nbr = 2 * mult * nbr + mult;
            }

        }

        return  nbr;

    }


    private static int calculateNbr(int nbrEx) {
        int mod2 = nbrEx % 2;
        if (nbrEx == 2)
        {
            return 4;
        }
        else if (nbrEx == 1)
        {
            return 2;
        }
        else if (mod2 == 0) {
            int prev =  calculateNbr(nbrEx/2);
            return prev*prev; // mod 1000009
        }
        else {//if ((nbrEx-1) %2 == 0) {
            int prev = calculateNbr((nbrEx-1)/2);
            return 2*(prev)*(prev); // example 2^11 = 2^1*2^5*2^5
        }
//        else {
//            return agg * Math.pow(2, nbrEx);
////        }

    }
}
