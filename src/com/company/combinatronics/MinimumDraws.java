package com.company.combinatronics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 1/31/15.
 */
public class MinimumDraws {
    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static int nbrEx;

    public static void NbrOfSubsets(Input2 input) {

        nbrEx = input.nextInt();
        double nbrEx2 = (double) nbrEx;
    }

    public static void main(String[] args) throws IOException {

        Input    in = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/draws/1.txt")));
        int nbr = in.nextInt();
        for (int i = 0; i < nbr; i++) {
            int result = draws(in);
            System.out.println(result);
        }
    }

    private static int draws(Input in) throws IOException {
        int nbrSocks = in.nextInt();
        return nbrSocks + 1;
    }

}
