package com.company;

/**
 * Created by federicobrubacher on 11/21/14.
 */
public class knapsack2 {
    public static int[] itemWeights = { 1, 9, 1, 1};
    public static int[] itemValues = { 1, 19, 3, 4};

    public static int calculateKnapsack(int agg, int item, int weight) {
        if (item > 3)
            return agg;
        if (itemWeights[item] > weight) {
            return calculateKnapsack(agg, item+1, weight);
        }
        return Math.max(calculateKnapsack(agg, item + 1, weight),
                calculateKnapsack(agg + itemValues[item], item + 1, weight - itemWeights[item]));
    }
    public static void main(String[] args) {
        System.out.println("RESULTADO :" + calculateKnapsack(0, 0,  10));
    }


//    int currSeated[] = {1, 2, 0, 0, 1};
//
//    // algo  : if  I start from the end =>
//    public static int calculateMin(int agg, int currShyLevel, int nbrSeated) {
//
//        int maxShy = 5;
//        for (int i = 0; i < maxShy ; i++) {
//
//            if ( agg < currShyLevel) {
//                calcMinRec(agg, currShyLevel);
//            }
//
//        }
//
//    }
//
//    // subproblem given that all my levels have risen up
//    // should I rise up ?
//    public static int calcMinRec(int curr_agg, int currSyLevel, int[] currSeated) {
//
//            // aggrego en este nivel o agrego en el siguiente ?
//
//
//        Math.min(agg, )
//
//
//
//
//    }

}


