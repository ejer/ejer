package com.company;

public class Tuple {
    public final Integer x;
    public final Integer y;
    public Tuple(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Tuple sum(Tuple toSum)
    {
        return new Tuple(this.fst() + toSum.fst(), this.snd() + toSum.snd());
    }
    public Integer fst()
    {
        return this.x;
    }

    public Integer snd() {
        return this.y;
    }

}
