package com.company.algorithmsCourse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

/**
 * Created by federicobrubacher on 10/10/15.
 */
public class CountBits {

    public static void main(String[] args) {
        CountBits c = new CountBits();
         int[] a = { -5, 5, 5, -1, 0, -5, -1, -1, -3, 3, 2, 5, 1, 0, 5, -1 };
        ArrayList<Integer> ls = new ArrayList<>();
        for (int p : a) {
             ls.add(p);
         }
        c.threeSum(ls);
//        int ans = 0, n = A.size();
//
//        //traverse over all bits
//        for (int i = 0; i < 31; i++) {
//
//            //count number of elements with ith bit = 0
//            LL cnt = 0;
//            for (int j = 0; j < n; j++)
//                if ((A[j] & (1 << i))) cnt++;
//
//            //add to answer cnt*(n-cnt)*2
//            ans += (cnt * ((LL) n - cnt) * 2) % MOD;
//            if (ans >= MOD) ans -= MOD;
//
//        }
//
//        return ans;
    }

    public ArrayList<ArrayList<Integer>> threeSum(ArrayList<Integer> a) {

        int end = a.size() - 1;
        Collections.sort(a);


        ArrayList<ArrayList<Integer>> lls = new ArrayList<>();

        TreeSet<Integer> integers = new TreeSet<Integer>(a);


        ArrayList<Integer> b = new ArrayList<>(integers);
        
        if (a.size() < 3) return lls;

        for (int i = 0; i < end - 1; i++) {
            if (a.get(i) == a.get(i + 1)) continue;
            int j = i + 1;
            while (j < end) {
                int begVal = a.get(i);
                int nextVal = a.get(j);
                int endVal = a.get(end);
                if (nextVal == a.get(j + 1)) j++;
                if (begVal + nextVal + endVal > 0) {
                    end = end - 1;
                    while (a.get(end) == a.get(end - 1)) end--;
                } else if (begVal + nextVal + endVal < 0 && end < (a.size() - 1)) {
                    end++;

                    while(end <= a.size()-2) {
                        if (a.get(end) == a.get(end+1)) {
                            end++;
                        } else {
                            break;
                        }
                    }
                    //if (end != a.size()-1) end++;
                } else if (begVal + nextVal + endVal == 0) {
                    ArrayList<Integer> ls = new ArrayList<>();
                    ls.add(begVal);
                    ls.add(nextVal);
                    ls.add(endVal);
                    lls.add(ls);
                    break;
                } else {
                    j++;
                }
            }
        }
        return lls;

    }
}
