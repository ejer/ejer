package com.company.algorithmsCourse;

import java.io.*;

/**
 * Created by federicobrubacher on 2/4/15.
 */
public class CountInversions {
     static   int[] input;
    static long nbrReversions = 0;
    public static int mergeSort( int begin, int end, int[] output) {
        if (end - begin < 2)
            return 0;
        int middle = (begin+end)/2;
        mergeSort(begin, middle, output);
        mergeSort(middle, end, output);
        merge( begin, middle, end, output);
        copyArray(input, begin, end, output);
        return 0;
    }

    private static void copyArray(int[] input, int beg, int end ,int[] output) {
        for (int i = beg; i < end; i++) {
            input[i] = output[i];

        }
    }


    public static void printArr(int[] args) {
        for (int arg : args) {
            System.out.print(arg);
        }
    }

    //  left half is input[iBegin :iMiddle-1]
    // right half is input[iMiddle:iEnd-1   ]
    private static void merge(int begin, int middle, int end, int[] output) {
        int i0 = begin;
        int i1 = middle;
        for (int j = begin; j < end ; j++) { // go to every element in the coll
            // we put the left run if i0 is less than middle and either the right run terminated or it has not terminated
            // but the element of the left is less than the element of the right
            if (i0 < middle && (i1>= end || input[i0] < input[i1] )) { //
                output[j] = input[i0];
                i0 = i0 +1;
            } else {
                output[j] = input [i1];
                i1 = i1 +1;
                if (i1 <= end)
                {
                       if (input[i0] > input[i1-1]) // Check that there was a inversion , the run on the left had an element which value was greater than a value in the right run
                            nbrReversions += (middle -i0);
                }

            }

        }
    }


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {

        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/1.txt")));
        input = new int[100000];
        for (int i = 0; i < input.length; i++) {
            input[i] = in3.nextInt();
        }
//        input = new int[] { 6, 3, 8, 2, 5, 1, 9};
        mergeSort(0, input.length, new int[input.length]);
        System.out.println(nbrReversions);
//        for (int i = 0; i < input.length; i++) {
//            System.out.print(input[i]);
//
//        }

    }
}
