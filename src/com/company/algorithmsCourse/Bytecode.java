package com.company.algorithmsCourse;

/**
 * Created by federicobrubacher on 8/2/15.
 */
public class Bytecode {
    /* tricks :
    0110 * 2  = 1100
    0100
     */
    public static void main(String[] args) {
        int a = 9;
        int b = ~0;
        byte c = 0b1111111;

        int d = a^c;
        boolean e = getBit(5, 5);
        int cleared  = clearBitsMSBthroughI(9,2);
        int pruebaShift = (5 << 2);
        byte prueba = 0b00001010;
        System.out.print(pruebaShift-2);
        System.out.print(prueba);



    }

    static int clearBitsMSBthroughI(int num, int i) {
        int mask = (1 << i);
        byte b = (byte) mask;

//        System.out.println(b);

        mask = mask-1;

//        System.out.println(mask);
        return num & mask;
    }

    static int clearBit(int numb, int i) {

        int mask = ~(1 << i);
        return numb & mask;

    }

    static boolean getBit(int num, int i) {
        return ((num & (1 << i)) !=0);
    }

}
