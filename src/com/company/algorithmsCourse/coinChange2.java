package com.company.algorithmsCourse;

import java.util.Scanner;

/**
 * Created by federicobrubacher on 8/7/15.
 */
public class coinChange2 {


    static int[][] cache;
    static int M;
    static int N;
    public static int giveChange(int[] input, int pos, int currSum) {
        if (currSum == M) {
            return 1;
        } else if ( currSum > M) {
            return 0;
        } else if (pos == input.length) {
            return 0;
        } else if (cache[pos][currSum] > 0) {
            return cache[pos][currSum];
        }
        else {

            int in = input[pos];
            return cache[pos][currSum] = giveChange(input, pos + 1, currSum) +
                    giveChange(input, pos, currSum + in);
        }
    }


    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }


    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Input2 in = new Input2();
        int coins[] = {1,2,3};

        N =  3;
        M = 4;
        cache = new int[N][M];
//        for (int i = 0; i < N; i++) {
//            coins[i] = in.nextInt();
//        }
        int result = giveChange(coins, 0, 0);
        System.out.print(result);
    }
}
