package com.company.algorithmsCourse.djikstra;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;


public class Kruskal {

    public static Graph normal = new Graph();

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n".indexOf(c) == -1) {
                    sb.append((char) c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char) c);
            }
            return sb.toString();
        }

        public HashMap<Integer, Collection<Integer>> adjList() throws IOException {

            HashMap<Integer, Collection<Integer>> adjLists = new HashMap<Integer, Collection<Integer>>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int head = Integer.parseInt(nbrs[0]);
                    List<Integer> adjs = new ArrayList<Integer>();
                    for (int i = 1; i < nbrs.length; i++) {
                        adjs.add(Integer.parseInt(nbrs[i]));
                    }
                    adjLists.put(head, adjs);
                }
            }
            return adjLists;
        }


//        public void generateGraph(Graph graph, List<Integer> startVertices, List<Integer[]> nodes) {
//
//            for (int i = 0; i < startVertices.size(); i++) {
//
//                Integer[] adjNodes = nodes.get(i);
//                Vertex[] adjList = new Vertex[adjNodes.length];
//                Edge[] adjListEdges = new Edge[adjNodes.length];
//                Vertex from = graph.get(startVertices.get(i));
//                Boolean add = false;
//                if (from == null) {
//                    from = new Vertex(startVertices.get(i));
//                    add = true;
//                }
//
//                for (int j = 0; j < adjNodes.length; j++) {
//                    Vertex into = graph.get(adjNodes[j]);
//                    Boolean add1 = false;
//                    if (into == null) {
//                        add1 = true;
//                        into = new Vertex(adjNodes[j]);
//                    }
//
//                    adjList[j] = into;
//
//                    Edge weightedEdge = new Edge(from, into);
//
//                    adjListEdges[j] = weightedEdge;
//
//                    if ( add1 ) {
//                        graph.addVertex( into );
//                    }
//
//
//                }
//
//                from.adjList = Arrays.asList(adjList);
//                from.outgoing = Arrays.asList(adjListEdges);
//                if (add) {
//                    graph.addVertex(from);
//                }
//            }
//
//        }


//        public void readTuples() throws IOException {
//            List<Integer[]> connectList = new ArrayList<Integer[]>();
//            List<Integer> startVertexList = new ArrayList<Integer>();
//
//            in.readLine(); // skip first
//
//            while (true) {
//                String line = in.readLine();
//                if (line == null) {
//                    break;
//                } else {
//                    String nbrs[] = line.split("\\t");
//                    int a = Integer.parseInt(nbrs[0]);
//                    int b = Integer.parseInt(nbrs[1]);
//                    startVertexList.add(a);
//                    startVertexList.add(b);
//                }
//            }
//            generateGraph(normal, startVertexList, connectList);
//        }
//

        public void readTuples() throws IOException {

            int nbrLines = Integer.parseInt(in.readLine());

            for (int i = 0; i < nbrLines; i++) {

                String s = in.readLine();

                String[] split = s.split(" ");

                int from = Integer.parseInt(split[0]);
                int into = Integer.parseInt(split[1]);

                Boolean addIn = false;
                Boolean addOut = false;
                Vertex fromVertex = normal.get(from);
                if (fromVertex == null) {
                    fromVertex = new Vertex(from);
                    addIn = true;
                }

                Vertex toVertex = normal.get(into);
                if (toVertex == null) {
                    toVertex = new Vertex(into);
                    addOut = true;

                }

                int weight = Integer.parseInt(split[2]);
                WeightedEdge edge = new WeightedEdge(fromVertex, toVertex, weight);

                fromVertex.addOutgoing(edge);
                toVertex.addIncoming(edge);
                normal.addWeightedEdge(edge);

                if (addIn) {
                    normal.addVertex(fromVertex);
                }
                if (addOut) {
                    normal.addVertex(toVertex);
                }


            }
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static class WeightedEdge extends Edge implements Comparable {

        Integer weight;

        public WeightedEdge(Vertex from, Vertex into, Integer weight) {
            super(from, into);
            this.weight = weight;
        }

        @Override
        public int compareTo(Object edge2) {
            WeightedEdge edge21 = (WeightedEdge) edge2;
            if (this.weight > edge21.weight) {
                return 1;
            } else if (this.weight == edge21.weight) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    public static class Graph<V extends Edge<E>, E extends Vertex<V>> {

        ArrayList<Vertex> vertexes = new ArrayList<Vertex>();
        HashMap<Integer, Vertex> integerToNode = new HashMap<Integer, Vertex>();
        ArrayList<WeightedEdge> edges = new ArrayList<WeightedEdge>();


        public Graph() {
        }

        public void addWeightedEdge(WeightedEdge edge) {
            edges.add(edge);
        }

        public void addVertex(Vertex vertex) {
            vertexes.add(vertex);
            integerToNode.put(vertex.key, vertex);
            vertex.adj = vertex.adjList.iterator();
        }


        public boolean contains(Integer integer) {
            if (integerToNode.containsKey(integer))
                return true;
            return false;
        }

        public Vertex get(Integer integer) {
            return integerToNode.get(integer);
        }

        public int size() {
            return this.vertexes.size();
        }

        public Edge[] getEdgesWith(int s) {
            Vertex vertex = integerToNode.get(s);
            return (Edge[]) vertex.getOutgoing();
        }

        public ArrayList<WeightedEdge> getWeightedEdges() {
            return edges;
        }


    }

    public static class Vertex<Edge> implements Comparable<Vertex> {
        public int key;
        public List<Vertex> adjList;
        public boolean marked = false;
        public boolean marked2 = false;
        public List<WeightedEdge> outgoing = new ArrayList<WeightedEdge>();
        public List<WeightedEdge> incoming = new ArrayList<WeightedEdge>();

        public Iterator<Vertex> adj;

        public WeightedEdge[] getOutgoing() {
            return (WeightedEdge[]) outgoing.toArray();
        }

        public Edge[] getIncoming() {
            return (Edge[]) incoming.toArray();
        }

        public void addIncoming(WeightedEdge edge) {
            incoming.add(edge);
        }

        public void addOutgoing(WeightedEdge edge) {
            outgoing.add(edge);
        }

        public void setIncoming(List<WeightedEdge> incoming) {
            this.incoming = incoming;
        }

        public void setOutgoing(List<WeightedEdge> outgoing) {
            this.outgoing = outgoing;
        }

        public Vertex(int key, List<Vertex> adjList) {
            this.adjList = adjList;
            this.key = key;
        }

        public Vertex(Integer integer) {
            this.key = integer;
            this.adjList = new ArrayList<Vertex>();
        }

        public void addVertexToList(Vertex adj) {
            adjList.add(adj);
        }

        public Iterator<Vertex> genIter() {
            if (adj == null) {
                adj = adjList.iterator();
            }
            return adj;
        }

        @Override
        public int compareTo(Vertex o) {
            if (this.key >= o.key)
                return 1;
            return -1;
        }
    }

    public static class Edge<Vertex> {
        public Vertex from;
        public Vertex into;

        public Vertex getFrom() {
            return from;
        }

        public Vertex getInto() {
            return into;
        }

        public Edge(Vertex from, Vertex into) {
            this.from = from;
            this.into = into;
        }
    }

    /**
     * Created by federicobrubacher on 4/1/15.
     * # algo :
     * ordeno todos los edges , y luego voy desde mayor a menor y busco si no tiene ciclos en el grafo
     * o sea si ya no pertenece a uno de los componentes
     * <p/>
     * good pointer es que Kruskal naive es O(mn) for i to n : (bst to find if T U {i} has no cycles)
     * union - find :
     */


    static HashSet<WeightedEdge> currentMst = new HashSet<WeightedEdge>();

    public static void kruskal(Graph g, int k) {

        Collections.sort(g.getWeightedEdges());
        int x = 1;

        for (Object weightedE : g.getWeightedEdges()) {
            if (x <= k) {
                WeightedEdge weightedEdge = (WeightedEdge) weightedE;
                currentMst.add(weightedEdge);

                HashSet<Vertex> testCycles = new HashSet<Vertex>();
                testCycles.add((Vertex) weightedEdge.getFrom()); // start from the from vertex and check in currentMSt if there are edges that might make a cycle
                boolean cycle = doDfsHasCycles(weightedEdge, testCycles);
                if (cycle) {
                    currentMst.remove(weightedEdge);
                } else {
                    x += 1;
                }
            } else {
                break;
            }

        }

        for (WeightedEdge weightedEdge : currentMst) {

            g.edges.remove(weightedEdge);

        }


        for (Object o : g.getWeightedEdges()) {
            WeightedEdge weightedEdge = (WeightedEdge) o;
            HashSet<Vertex> testCycles = new HashSet<Vertex>();
            testCycles.add((Vertex) weightedEdge.getFrom());
            boolean hasCycle = doDfsHasCycles(weightedEdge, testCycles);
            if (!hasCycle) {
                System.out.println("result " + weightedEdge.weight);
                break;
            }
        }
    }


    // por cada uno de los outgoing edges en el outgoing vertex ver si tiene ciclos
    private static boolean doDfsHasCycles(WeightedEdge weightedEdge, HashSet<Vertex> seenTree) {
        Vertex into = (Vertex) weightedEdge.getInto();

        if (seenTree.contains(into)) {
            return true;
        } else {
            seenTree.add(into);
            List<WeightedEdge> combined = new ArrayList<WeightedEdge>();
//            combined.addAll(into.incoming);
            combined.addAll(into.outgoing);
            for (Object o : combined) {
                WeightedEdge outgoingEdge = (WeightedEdge) o;
                if (currentMst.contains(outgoingEdge)) {
                    return doDfsHasCycles(outgoingEdge, seenTree);
                }
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        Input in3 = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera2/21text.txt")));
        in3.readTuples();
        kruskal(normal, 2);
    }
}
