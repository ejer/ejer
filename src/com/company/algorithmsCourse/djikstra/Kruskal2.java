package com.company.algorithmsCourse.djikstra;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;


public class Kruskal2 {

    public static Graph normal = new Graph();
    public static int nbrClusters;

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n".indexOf(c) == -1) {
                    sb.append((char) c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char) c);
            }
            return sb.toString();
        }

        public HashMap<Integer, Collection<Integer>> adjList() throws IOException {

            HashMap<Integer, Collection<Integer>> adjLists = new HashMap<Integer, Collection<Integer>>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int head = Integer.parseInt(nbrs[0]);
                    List<Integer> adjs = new ArrayList<Integer>();
                    for (int i = 1; i < nbrs.length; i++) {
                        adjs.add(Integer.parseInt(nbrs[i]));
                    }
                    adjLists.put(head, adjs);
                }
            }
            return adjLists;
        }


//        public void generateGraph(Graph graph, List<Integer> startVertices, List<Integer[]> nodes) {
//
//            for (int i = 0; i < startVertices.size(); i++) {
//
//                Integer[] adjNodes = nodes.get(i);
//                Vertex[] adjList = new Vertex[adjNodes.length];
//                Edge[] adjListEdges = new Edge[adjNodes.length];
//                Vertex from = graph.get(startVertices.get(i));
//                Boolean add = false;
//                if (from == null) {
//                    from = new Vertex(startVertices.get(i));
//                    add = true;
//                }
//
//                for (int j = 0; j < adjNodes.length; j++) {
//                    Vertex into = graph.get(adjNodes[j]);
//                    Boolean add1 = false;
//                    if (into == null) {
//                        add1 = true;
//                        into = new Vertex(adjNodes[j]);
//                    }
//
//                    adjList[j] = into;
//
//                    Edge weightedEdge = new Edge(from, into);
//
//                    adjListEdges[j] = weightedEdge;
//
//                    if ( add1 ) {
//                        graph.addVertex( into );
//                    }
//
//
//                }
//
//                from.adjList = Arrays.asList(adjList);
//                from.outgoing = Arrays.asList(adjListEdges);
//                if (add) {
//                    graph.addVertex(from);
//                }
//            }
//
//        }


//        public void readTuples() throws IOException {
//            List<Integer[]> connectList = new ArrayList<Integer[]>();
//            List<Integer> startVertexList = new ArrayList<Integer>();
//
//            in.readLine(); // skip first
//
//            while (true) {
//                String line = in.readLine();
//                if (line == null) {
//                    break;
//                } else {
//                    String nbrs[] = line.split("\\t");
//                    int a = Integer.parseInt(nbrs[0]);
//                    int b = Integer.parseInt(nbrs[1]);
//                    startVertexList.add(a);
//                    startVertexList.add(b);
//                }
//            }
//            generateGraph(normal, startVertexList, connectList);
//        }
//

        public void readTuples() throws IOException {

            nbrClusters = Integer.parseInt(in.readLine());

            for (int i = 0; i < 100000; i++) {

                String s = in.readLine();
                if ( s== null)
                    break;

                String[] split = s.split(" ");

                int from = Integer.parseInt(split[0]);
                int into = Integer.parseInt(split[1]);

                Boolean addIn = false;
                Boolean addOut = false;
                Node fromVertex = normal.get(from);
                if (fromVertex == null) {
                    fromVertex = new Node(from);
                    addIn = true;
                }

                Node toVertex = normal.get(into);
                if (toVertex == null) {
                    toVertex = new Node(into);
                    addOut = true;

                }

                int weight = Integer.parseInt(split[2]);
                WeightedEdge edge = new WeightedEdge(fromVertex, toVertex, weight);

                fromVertex.addOutgoing(edge);
                toVertex.addIncoming(edge);
                normal.addWeightedEdge(edge);

                if (addIn) {
                    normal.parents.put(fromVertex, fromVertex);
                    normal.addVertex(fromVertex);
                }
                if (addOut) {
                    normal.parents.put(toVertex, toVertex);
                    normal.addVertex(toVertex);
                }


            }
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static class WeightedEdge extends Edge implements Comparable {

        Integer weight;

        public WeightedEdge(Node from, Node into, Integer weight) {
            super(from, into);
            this.weight = weight;
        }

        @Override
        public int compareTo(Object edge2) {
            WeightedEdge edge21 = (WeightedEdge) edge2;
            if (this.weight > edge21.weight) {
                return 1;
            } else if (this.weight == edge21.weight) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    public static class Graph<V extends Edge<E>, E extends Node<V>> {

        ArrayList<Node> vertexes = new ArrayList<Node>();
        HashMap<Integer, Node> integerToNode = new HashMap<Integer, Node>();
        ArrayList<WeightedEdge> edges = new ArrayList<WeightedEdge>();
        HashMap<Node, Node> parents = new HashMap<Node, Node>();


        public Graph() {
        }

        public void addWeightedEdge(WeightedEdge edge) {
            edges.add(edge);
        }

        public void addVertex(Node vertex) {
            vertexes.add(vertex);
            integerToNode.put(vertex.key, vertex);
            vertex.adj = vertex.adjList.iterator();
        }


        public boolean contains(Integer integer) {
            if (integerToNode.containsKey(integer))
                return true;
            return false;
        }

        public Node get(Integer integer) {
            return integerToNode.get(integer);
        }

        public int size() {
            return this.vertexes.size();
        }

        public Edge[] getEdgesWith(int s) {
            Node vertex = integerToNode.get(s);
            return (Edge[]) vertex.getOutgoing();
        }

        public ArrayList<WeightedEdge> getWeightedEdges() {
            return edges;
        }


    }

    public static class Node<Edge> implements Comparable<Node> {
        public int key;
        public List<Node> adjList;
        public List<WeightedEdge> outgoing = new ArrayList<WeightedEdge>();
        public List<WeightedEdge> incoming = new ArrayList<WeightedEdge>();
        public int rank = 0;

        public Iterator<Node> adj;

        public WeightedEdge[] getOutgoing() {
            return (WeightedEdge[]) outgoing.toArray();
        }

        public Edge[] getIncoming() {
            return (Edge[]) incoming.toArray();
        }

        public void addIncoming(WeightedEdge edge) {
            incoming.add(edge);
        }

        public void addOutgoing(WeightedEdge edge) {
            outgoing.add(edge);
        }

        public void setIncoming(List<WeightedEdge> incoming) {
            this.incoming = incoming;
        }

        public void setOutgoing(List<WeightedEdge> outgoing) {
            this.outgoing = outgoing;
        }

        public Node(int key, List<Node> adjList) {
            this.adjList = adjList;
            this.key = key;
        }

        public Node(Integer integer) {
            this.key = integer;
            this.adjList = new ArrayList<Node>();
        }

        public void addVertexToList(Node adj) {
            adjList.add(adj);
        }

        public Iterator<Node> genIter() {
            if (adj == null) {
                adj = adjList.iterator();
            }
            return adj;
        }

        @Override
        public int compareTo(Node o) {
            if (this.key >= o.key)
                return 1;
            return -1;
        }
    }

    public static class Edge<Node> {
        public Kruskal2.Node from;
        public Kruskal2.Node into;

        public Kruskal2.Node getFrom() {
            return from;
        }

        public Kruskal2.Node getInto() {
            return into;
        }

        public Edge(Kruskal2.Node from, Kruskal2.Node into) {
            this.from = from;
            this.into = into;
        }
    }


    /**
     * 4/12/15
     * union - find
     * algo : 1) find : locate the parent from both , if parent1 == parent2 , pop another one
     * make find-parent O(n) Map<Node, Node>  initialized with self, inside the parent node add a rank attribute
     */

    public static void cluster(Graph g, int k) {
        Collections.sort(g.edges);
        int i = 0;
        while (nbrClusters > k) {
            WeightedEdge we = (WeightedEdge) g.edges.get(i);
            while (sameComponent(g, we.from, we.into)) {
                i += 1;
                we = (WeightedEdge) g.edges.get(i);
            }
            union(g, we.from, we.into);
            System.out.println("Cluster :" + we.from.key + ", " + we.into.key + " , " + we.weight);
            nbrClusters--;
            i++;
        }

        WeightedEdge we = null;
        for (int p = i; p < g.edges.size(); p++) {
            if (we == null) {
                we = (WeightedEdge) g.edges.get(p);
            }
            if (sameComponent(g, we.from, we.into)) {
                p += 1;
                we = (WeightedEdge) g.edges.get(p);
            } else {
                break;
            }
        }
        System.out.println("from : " + we.from.key);
        System.out.println("into : " + we.into.key);
        System.out.println("weight : " + we.weight);

    }

    private static void union(Graph g, Node from, Node into) {
        Node parentFrom = findParent(g, from);
        Node parentInto = findParent(g, into);
        // to find the nodes that have the same parent
        if (parentFrom.rank >= parentInto.rank) {
            g.parents.put(parentInto, parentFrom);
            parentFrom.rank += 1;
        } else {
            g.parents.put(parentFrom, parentInto);
            parentInto.rank += 1;
        }
    }

    private static boolean sameComponent(Graph g, Node from, Node into) {
        return findParent(g, from) == findParent(g, into);
    }

    private static Node findParent(Graph g, Node from) {
        Node currParent = (Node) g.parents.get(from);
        if (currParent == from) {
            return currParent;
        } else {
            return findParent(g, currParent);
        }
    }


    public static void main(String[] args) throws IOException {
        Input in3 = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera2/21text.txt")));
        in3.readTuples();
        cluster(normal, 4);
    }
}
