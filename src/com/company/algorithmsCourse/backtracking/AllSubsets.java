package com.company.algorithmsCourse.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by federicobrubacher on 9/11/15.
 */
public class AllSubsets {
    public List<List<Integer>>  subsetsHelper(int[] nums ) {

        List<List<Integer>> ls  = new ArrayList<List<Integer>>();
        List<Integer> l  = new ArrayList<>();
        l.add(nums[0]);
        ls.add(l);


        for (int i = 1; i < nums.length; i++) {
            List<List<Integer>> newLs  = new ArrayList<List<Integer>>();
            for(List<Integer> l1 : ls) {
                List<Integer> temp  = new ArrayList<>();
                temp.addAll(l1);
                temp.add(nums[i]);
                newLs.add(temp);
            }
            ls.addAll(newLs);
        }

        return ls;
    }



    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();

        result.add(new ArrayList<Integer>());
        for (int i = 0; i < nums.length; i++) {
            int[] excluding = Arrays.copyOfRange(nums, i, nums.length);
            List<List<Integer>> lists = subsetsHelper(excluding);
            result.addAll(lists);
        }

        return result;

    }

    public static void main(String[] args) {
        AllSubsets a = new AllSubsets();
        int[] nums = {1,2,3};
        List<List<Integer>> subsets = a.subsets(nums);
        System.out.print(subsets);
    }
}
