package com.company.algorithmsCourse.backtracking;

import java.util.*;

/**
 * Created by federicobrubacher on 7/9/15.
 */
public class PalindromeIndex {

    public static void main(String[] args) {

        List<Integer> l = new ArrayList<>();
        Iterator<Integer> iterator = l.iterator();

//        Map<Character, Character[]> map  = new HashMap<>();
        Input2 in = new Input2();
        int times = in.nextInt();
        String next;

        for (int i = 0; i < times; i++) {

            next = in.next();
//            next = "fgnfnidynhxebxxxfmxixhsruldhsaobhlcggchboashdlurshxixmfxxxbexhnydinfngf";
            int result = testPalidrome(next, 0, next.length() - 1);
            System.out.println(result);
        }


    }

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }

    private static int testPalidrome(String next, int f, int l) {

        /* algo :

         */


        int r1 = 0;
        int r2 = 0;
        int result = -20;

        while (f < l) {
            char c1 = next.charAt(f);
            char c2 = next.charAt(l);
            if (c1 != c2) {
                r1 = testrem(next, f + 1, l, 0);
                r2 = testrem(next, f, l - 1, 1);
                break;
            }
            f++;
            l--;
        }


        if (r1 == 0 && r2 == 0) {
            if (result == -20) {
                return -1;
            }
            return result;
        } else if (r1 != -20) {
            return r1;
        } else {
            return r2;
        }


    }

    private static int testrem(String next, int i, int l, int i1) {
        char c1 = next.charAt(i);
        char c2 = next.charAt(l);
        if (c1 != c2) {
            return -20;
        }
        if (i1 == 0) {
            return i - 1;
        } else {
            return l + 1;
        }
    }

}
