package com.company.algorithmsCourse.backtracking;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by federicobrubacher on 11/22/15.
 */
public class Integer2Roman {

    public String intToRoman(int num) {
        String res = "";
        if (num == 0) {
            return res;
        }
        if (num > 3999) {
            return res;
        }
        Map<Integer, String> map = new TreeMap<>();
        map.put(1, "I");
        map.put(4, "IV");
        map.put(5, "V");
        map.put(9, "IX");
        map.put(10, "X");
        map.put(40, "XL");
        map.put(50, "L");
        map.put(90, "XC");
        map.put(100, "C");
        map.put(400, "CD");
        map.put(500, "D");
        map.put(900, "CM");
        map.put(1000, "M");

        if (num > 1000) {
            int tmp = (num / 1000) * 1000;
            num = num % 1000;
            res = intToRomanHelper(tmp, "C", 100, map);
        }

        if (num > 100) {
            int tmp = (num / 100) * 100;
            num = num % 100;
            res += intToRomanHelper(tmp, "X",10, map);
        }

        if (num > 10) {
            int tmp = (num / 10) * 10;
            num = num % 10;
            res += intToRomanHelper(tmp, "X",10, map);
        }


        res += intToRomanHelper(num % 10, "I",1, map);
        return res;
    }


    public String intToRomanHelper(int num, String toAdd, Integer valToAdd, Map<Integer, String> map) {


        // IV cuatro 44 ILIV
        //LI

        String res = "";
        String prev = "";
        Integer prevInt = 0;

        for (Map.Entry<Integer, String> curr : map.entrySet()) {

            if (curr.getKey() == num) {
                return curr.getValue();
            }

            if (curr.getKey() > num) {
                break;
            } else {
                prev = curr.getValue();
                prevInt = curr.getKey();
            }

        }

        res = res + prev;
        for (int i = 0; i < (num - prevInt); i+=valToAdd) {
            res += toAdd;
        }

        return res;

    }

    public static void main(String[] args) {
        Integer2Roman i2R = new Integer2Roman();
        System.out.print(i2R.intToRoman(124));
    }


}
