package com.company.algorithmsCourse.backtracking;

import java.util.*;

/**
 * Created by federicobrubacher on 11/23/15.
 */
public class ThreeSum {
    public void addToRes(int[] nums, int i, int j, int k) {
        List<Integer> ls = new ArrayList<>();
        ls.add(nums[i]);
        ls.add(nums[j]);
        ls.add(nums[k]);
        Collections.sort(ls);
        res.add(ls);
    }

    public ArrayList<Integer> intersect(final List<Integer> a, final List<Integer> b) {
        ArrayList<Integer> ls = new ArrayList<>();
        int m = a.size();
        int n = b.size();
        int j = 0;
        int k = 0;
        while ( j < m && k < n) {
            if (a.get(j).equals(b.get(k))) {
                ls.add(a.get(j));
                j++;
                k++;
            } else if (a.get(j) > b.get(k)) {
                k++;
            } else {
                j++;
            }
        }
        return ls;
    }

    public int genRandom(int start, int end) {
        Random r = new Random();
        int nextInt = r.nextInt(end - start + 1);
        return nextInt;
    }

    public List<List<Integer>> res = new ArrayList<>();
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums.length < 3) return res;
        if (nums.length == 3) {
            if ((nums[0] + nums[1] + nums[2]) == 0)
                addToRes(nums, 0,1,2);
            return res;
        }
        Arrays.sort(nums);
        int m = nums.length-1;

        for(int i = 0; i < m-2; i++) {
            while (i < m-2 && i > 0 && nums[i] == nums[i-1]) i++;
            int j = i+1;
            while(j < m-1 && nums[j] == nums[j+1]) j++;
            int k = m;
            while (k > j+1 && nums[k] == nums[k-1]) k--;
            while (j < k) {
                int curr_sum = nums[i] + nums[j] + nums[k];
                if (curr_sum == 0) {
                    addToRes(nums, i,j,k);
                    j++;
                    k--;
                } else if (curr_sum > 0) {
                    k--;
                } else {
                    j++;
                }
            }
        }

        return res;

    }
    public static void main(String[] args) {
        ThreeSum t = new ThreeSum();
//        int[] three = {7,-1,14,-12,-8,7,2,-15,8,8,-8,-14,-4,-5,7,9,11,-4,-15,-6,1,-14,4,3,10,-5,2,1,6,11,2,-2,-5,-7,-6,2,-15,11,-6,8,-4,2,1,-1,4,-6,-15,1,5,-15,10,14,9,-8,-6,4,-6,11,12,-15,7,-1,-9,9,-1,0,-4,-1,-12,-2,14,-9,7,0,-3,-4,1,-2,12,14,-10,0,5,14,-1,14,3,8,10,-8,8,-5,-2,6,-11,12,13,-7,-12,8,6,-13,14,-2,-5,-11,1,3,-6};
//        List<List<Integer>> lists = t.threeSum(three);
        List<Integer> a = Arrays.asList(1000);
        List<Integer> b = Arrays.asList(1000);
        ArrayList<Integer> intersect = t.intersect(a, b);

        System.out.print(intersect);
    }
}
