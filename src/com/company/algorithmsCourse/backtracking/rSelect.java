package com.company.algorithmsCourse.backtracking;

/**
 * Created by federicobrubacher on 11/28/15.
 */
public class rSelect {

    public static void swap(int[] nums, int a, int b) {
        int temp = nums[a];
        nums[a] = nums[b];
        nums[b] = temp;
    }

    public int partition(int[] nums, int l, int r) {
        swap(nums, l, r);
        int pivot = nums[l];
        int i = l+1;
        for (int j = l+1; j <= r; j++) {
            if (nums[j] < pivot) {
                swap(nums, i,j);
                i+=1;
            }
        }

        swap(nums, l, i-1);
        return i-1;

    }


    public int rSelect(int[] nums, int target, int currOrderS, int l, int r)
    {
        int n = nums.length;
        if (n == 0) return 0;
        if(n == 1) {
            return nums[0];
        }
        int orderS =  partition(nums,l,r);
        if (nums[orderS] == target)
        {
            return orderS;
        } else if (target > nums[orderS]) {
            return rSelect(nums, target, orderS, orderS+1, r);
        } else {
            return rSelect(nums, target, orderS, l, orderS-1);
        }

    }


    public int findKthLargest(int[] nums, int k) {
        return rSelect(nums, k, 0, 0, nums.length-1);


    }

    public static void main(String[] args) {
        rSelect r  = new rSelect();
        int[] nums = {99, 99};
        r.findKthLargest(nums, 1);
    }
}
