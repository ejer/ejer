package com.company.algorithmsCourse.backtracking;

import java.util.Arrays;

/**
 * Created by federicobrubacher on 9/26/15.
 */
public class WiggleSort {

    public void swap(int[] nums, int beg, int end) {
        int tmp = nums[beg];
        nums[beg] = nums[end];
        nums[end] = tmp;
    }

    public void printarr(int[] nums) {
        for (int num : nums) {
            System.out.print(num + ",");
        }
    }

    // 1 2 3 4 5 6
    // 1 6 3 4 5 2
    // 1 6 2 3 4 5
    // 1 6 2 5 4 3
    // 1 6 2 5 3 4

    public void wiggleSort(int[] nums) {
        if (nums.length == 0) return;
        int beg = 0;
        int end = nums.length - 1;
        Arrays.sort(nums);
        while (beg < end) {
            swap(nums, beg + 1, end);
            Arrays.sort(nums, beg + 2, nums.length);

            beg += 2;
        }

    }

    public static void main(String[] args) {
        int[] nums = { 3, 5, 2, 1, 6, 4 };
        WiggleSort w = new WiggleSort();
        w.wiggleSort(nums);
        w.printarr(nums);
//        whi

        int pepe  = 7/2;
        System.out.print(pepe);
    }
}
