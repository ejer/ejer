package com.company.algorithmsCourse.backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicobrubacher on 9/7/15.
 */
public class EightQueen2 {

    class Solution {
        /**
         * Get all distinct N-Queen solutions
         * @param n : The number of queens
         * @return: All distinct solutions
         * For example, A string '...Q' shows a queen on forth position
         */
        List<ArrayList<String>> solveNQueens(int n) {
            // write your code here
            List<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
            if (n <= 0) {
                return result;
            }
            search(n, new ArrayList<Integer>(), result);
            return result;
        }

        /**
         * n : total rows / columns
         * cols : <row : queen column position>
         * result : <row : placement sting>
         */
        private void search(int n, ArrayList<Integer> cols, List<ArrayList<String>> result) {
            if (cols.size() == n) {
                result.add(drawChessboard(cols));
                return;
            }

            for (int col = 0; col < n; col++) {
                if (!isValid(cols, col)) {
                    continue;
                }
                cols.add(col);
                search(n, cols, result);
                cols.remove(cols.size() - 1);
            }
        }


        private boolean isValid(ArrayList<Integer> cols, int col) {
            int row = cols.size();
            for (int i = 0; i < row; i++) {
                // same column
                if (cols.get(i)== col)  {
                    return false;
                }
                // left-top to right-bottom
                if (i - cols.get(i) == row - col) {
                    return false;
                }
                // right-top to left-bottom
                if (i + cols.get(i) == row + col) {
                    return false;
                }
            }
            return true;
        }

        private ArrayList<String> drawChessboard(ArrayList<Integer> cols) {
            ArrayList<String> chessboard = new ArrayList<String>();
            for (int i = 0; i < cols.size(); i++) {
                chessboard.add(i, "");
                for (int j = 0; j < cols.size(); j++) {
                    if (j == cols.get(i)) {
                        chessboard.set(i, chessboard.get(i) + "Q");
                    } else {
                        chessboard.set(i, chessboard.get(i) + ".");
                    }
                }
            }

            return chessboard;
        }


    }
}
