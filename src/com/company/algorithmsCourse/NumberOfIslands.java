package com.company.algorithmsCourse;

/**
 * Created by federicobrubacher on 9/9/15.
 */
public class NumberOfIslands {


    public static void main(String args[]){

        int matrix[][] = {{1,1,0,1,0},
                {1,0,0,1,1},
                {0,0,0,0,0},
                {1,0,1,0,1},
                {1,0,0,0,0}
        };
        NumberOfIslands island = new NumberOfIslands();
        int count = island.numberOfIsland(matrix);
        System.out.println(count);
    }

    private int numberOfIsland(int[][] matrix) {
        int lengthi = matrix.length;
        int lengthj = matrix[0].length;
        boolean[][] visited = new boolean[lengthi][lengthj];

        int count = 0;

        for (int i = 0; i < lengthi; i++) {
            for (int j = 0; j < lengthj; j++) {

                if(matrix[i][j] == 1 && !visited[i][j]) {
                    visited[i][j] = true;
                    count++;

                    DFS(matrix, visited, i, j);

                }

            }

        }
        return count;
    }

    private void DFS(int[][] matrix, boolean[][] visited, int i, int j) {


        if (i<0 || j <0 || i == matrix.length || j == matrix[i].length) {
            return;
        }

        visited[i][j] = true;
        if (matrix[i][j] == 0) {
            return;
        }

        DFS(matrix, visited, i+1, j);
        DFS(matrix, visited, i, j+1);
        DFS(matrix, visited, i+1, j+1);
        DFS(matrix, visited, i-1, j-1);

    }
}
