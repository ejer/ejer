package com.company.algorithmsCourse;

import java.io.*;
import java.util.*;

/**
 * Created by federicobrubacher on 2/9/15.
 */
public class KargerMinCut {

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public HashMap<Integer,Collection<Integer>> adjList() throws IOException {

            HashMap<Integer, Collection<Integer>> adjLists = new HashMap<Integer, Collection<Integer>>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int head = Integer.parseInt(nbrs[0]);
                    List<Integer> adjs = new ArrayList<Integer>();
                    for (int i = 1; i < nbrs.length; i++) {
                        adjs.add(Integer.parseInt(nbrs[i]));
                    }
                    adjLists.put(head, adjs);
                }
            }
            return adjLists;
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    public static void main(String[] args) throws IOException {

        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/3test.txt")));
        HashMap<Integer, Collection<Integer>> adjLists = in3.adjList();
        int minCuts = countMinCuts(adjLists);
    }

    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    private static int countMinCuts(HashMap<Integer, Collection<Integer>> adjLists) {
        Set<Integer> vertexes = adjLists.keySet();
        while (vertexes.size() > 2) {
            Object[] vertexesArray = vertexes.toArray();
            int first = (Integer) vertexesArray[randInt(0, vertexesArray.length-1)]; //get a random element of the vertices in the graph

            Collection<Integer> firstConns = adjLists.get(first); // get the adjList
            Integer[] firstConnections = firstConns.toArray(new Integer[firstConns.size()]); // as an array
            int second = firstConnections[randInt(0, firstConnections.length-1)]; // get a random second vertex defining an edge
            List<Integer> secondConnections = (List<Integer>) adjLists.get(second); // get the second adjList

            HashSet<Integer> bothConnections = new HashSet<Integer>();

            bothConnections.addAll(firstConns);
            bothConnections.addAll(secondConnections); //let's get a set of all the connections of the new combined vertex
            if (bothConnections.contains(first)) { // remove self-loops
                bothConnections.remove(first);
            }
            adjLists.put(first, bothConnections); // keep first and add it to the graph

            vertexes.remove(second);
            adjLists.remove(second);

            for (Integer vertex : vertexes) {
                Iterator<Integer> iterator = adjLists.get(vertex).iterator(); // finally understood the seq abstraction ... look at this shit
                List<Integer> adjList = new ArrayList<Integer>();
                while(iterator.hasNext()) {
                    Integer next = iterator.next();
                    if (next != second) {
                        adjList.add(next);
                    } else {
                        adjList.add(first);
                    }
                }
                adjLists.put(vertex, adjList);
            }
        }

        return adjLists.keySet().size();
    }
}
