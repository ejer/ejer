package com.company.algorithmsCourse;

import java.util.Scanner;

/**
 * Created by federicobrubacher on 8/2/15.
 */
public class HackerRankImba {

    /*
    A DOTA game has N heroes, each with a distinct rank from [1..N]. In DOTA every formation is characterized as a permutation [1...N] of ranks of players.
A formation is Imba when the sum of ranks of every two consecutive players is less than or equal to (N+1). Given N, you are to print the lexicographically smallest permutation of ranks [1...N] that makes the formation Imba.
     */

    static int T = 0;
    static int N = 50;

    static class Input2 {
        Scanner scanner = new Scanner(System.in);

        public String next() {
            return scanner.next();
        }

        public int nextInt() {
            return Integer.parseInt(scanner.next());
        }
    }


    public static void main(String[] args) {

        Input2 input2 = new Input2();

        T = input2.nextInt();


        for (int r = 0; r < T; r++) {
            N = input2.nextInt();
            int j = N / 2;
            int P;
            if (N % 2 == 0) {
                P = N / 2;
            } else {
                P = N / 2 + 1;
                System.out.print(P);
                System.out.print(" ");

            }

            for (int i = P + 1; i <= N; i++) {
                System.out.print(j);
                System.out.print(" ");
                System.out.print(i);
                System.out.print(" ");
                j--;
            }
            System.out.println();
        }
    }


}
