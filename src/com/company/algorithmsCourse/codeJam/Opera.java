package com.company.algorithmsCourse.codeJam;

import java.io.*;
import java.util.PriorityQueue;

/**
 * Created by federicobrubacher on 4/10/15.
 */
public class Opera {

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {

        Input in = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/googcodejam/A-large-practice.in")));
        int T = in.nextInt();
        for (int i = 0; i < T; i++) {

            int nbrOfS = Integer.parseInt(in.next()) +1;

            int acc = 0;
            int[] nbrSeated = new int[nbrOfS];
            int friendsAdded = 0;
            char[] seats = in.next().toCharArray();
            for (int j = 0; j < nbrOfS; j++) {
                int currSSeated = Integer.parseInt(String.valueOf(seats[j]));

                nbrSeated[j] = currSSeated;

                int x = 0;
                int diff = j - acc;
                while ( diff > 0 && currSSeated != 0) {
                    int newSeated = (9 - nbrSeated[x]);
                    newSeated = diff < newSeated ? diff : newSeated; // use just the needed new spectators
                    nbrSeated[x] = nbrSeated[x] + newSeated;
                    x+=1; // new iteration
                    acc += newSeated;
                    friendsAdded += newSeated;
                    diff = diff - newSeated;
                }

                // when the people on j are SO then we move one
                acc += currSSeated;

            }

            System.out.println("Case #" + (i+1) + ":" + " " + friendsAdded);
        }
    }
}
