package com.company.algorithmsCourse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Created by federicobrubacher on 2/9/15.
 * Let G be a directed graph and S be an empty stack.
 * While S does not contain all vertices:
 * Choose an arbitrary vertex v not in S. Perform a depth-first search starting at v. Each time that depth-first search finishes
 * expanding a vertex u, push u onto S.
 * Reverse the directions of all arcs to obtain the transpose graph.
 * While S is nonempty:
 * Pop the top vertex v from S. Perform a depth-first search starting at v in the transpose graph.
 * The set of visited vertices will give the strongly connected component containing v;
 * record this and remove all these vertices from the graph G and the stack S.
 * Equivalently, breadth-first search (BFS) can be used instead of depth-first search.
 */

public class Scc2 {


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n".indexOf(c) == -1) {
                    sb.append((char) c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char) c);
            }
            return sb.toString();
        }

        public HashMap<Integer, Collection<Integer>> adjList() throws IOException {

            HashMap<Integer, Collection<Integer>> adjLists = new HashMap<Integer, Collection<Integer>>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int head = Integer.parseInt(nbrs[0]);
                    List<Integer> adjs = new ArrayList<Integer>();
                    for (int i = 1; i < nbrs.length; i++) {
                        adjs.add(Integer.parseInt(nbrs[i]));
                    }
                    adjLists.put(head, adjs);
                }
            }
            return adjLists;
        }


        public static Graph normal = new Graph();
        public static Graph reverse = new Graph();


        public void generateGraph(Graph graph, List<Integer[]> nodes) {
            for (Integer[] node : nodes) {
                if (graph.contains(node[0])) {
                    Node rootNode = graph.get(node[0]);
                    Node adjNode1;
                    if (graph.contains(node[1])) {

                        adjNode1 = graph.get(node[1]);
                    } else {
                        adjNode1 = new Node(node[1]);
                        graph.addNode(adjNode1);
                    }

                    rootNode.adjList.add(adjNode1);
                } else {
                    ArrayList<Node> adjNodes = new ArrayList<Node>();
                    Node adjNode0 = new Node(adjNodes, node[0]);
                    graph.addNode(adjNode0);

                    Node adjNode1;
                    if (graph.contains(node[1])) {

                        adjNode1 = graph.get(node[1]);
                    } else {
                        adjNode1 = new Node(node[1]);
                        graph.addNode(adjNode1);
                    }
                    adjNode0.addNodeToList(adjNode1);
                }
            }

        }

        public void readTuples() throws IOException {
            List<Integer[]> nodes = new ArrayList<Integer[]>();
            List<Integer[]> reverseList = new ArrayList<Integer[]>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int a = Integer.parseInt(nbrs[0]);
                    int b = Integer.parseInt(nbrs[1]);
                    Integer[] node = new Integer[]{a, b};
                    Integer[] rev = new Integer[]{b, a};
                    reverseList.add(rev);
                    nodes.add(node);
                }
            }
            generateGraph(normal, nodes);
            generateGraph(reverse, reverseList);
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static class Graph {

        ArrayList<Node> nodes = new ArrayList<Node>();
        HashMap<Integer, Node> integerToNode = new HashMap<Integer, Node>();


        public Graph() {
        }

        public void addNode(Node node) {
            nodes.add(node);
            integerToNode.put(node.key, node);
            node.adj = node.adjList.iterator();
        }


        public boolean contains(Integer integer) {
            if (integerToNode.containsKey(integer))
                return true;
            return false;
        }

        public Node get(Integer integer) {
            return integerToNode.get(integer);
        }
    }

    public static class Node implements Comparable<Node> {
        public int key;
        public List<Node> adjList;
        public boolean marked = false;
        public boolean marked2 = false;
        public Iterator<Node> adj;

        public Node(List<Node> adjList, int key) {
            this.adjList = adjList;
            this.key = key;
        }

        public Node(Integer integer) {
            this.key = integer;
            this.adjList = new ArrayList<Node>();
        }

        public void addNodeToList(Node adj) {
            adjList.add(adj);
        }

        public Iterator<Node> genIter() {
            if (adj == null) {
                adj = adjList.iterator();
            }
            return adj;
        }

        @Override
        public int compareTo(Node o) {
            if (this.key >= o.key)
                return 1;
            return -1;
        }
    }

//    public static class Graph extends Map<Integer, Set<Integer>> {
//
//    }


    public static void main(String[] args) throws IOException {
        Input in3 = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/4test.txt")));
        in3.readTuples();
        Collections.sort(in3.reverse.nodes);
        dfsLoop(in3.reverse);
        dfsLoop2(in3.normal);
//        dfsInverse(in3.reverse);
        System.out.print("pepe");
    }

    private static void dfsLoop2(Graph graph) {
        while (!sccStack.isEmpty()) {
            dfs2(graph);
        }
    }

    private static void dfsLoop(Graph graph) {
        t = 0; // number of nodes processed
        s = 0;  // current source vertex
        for (int i = graph.nodes.size() - 1; i >= 0; i--) {
            Node node = graph.nodes.get(i);
            s = node.key;
            dfs(graph, node);
        }
    }

    private static void dfs2(Graph reverse) {
        Stack<Node> stack = new Stack<Node>();
        Node node = sccStack.pop();

        node.marked2 = true;
        stack.push(node);
        for (Node adj : node.adjList) {
            adj.marked2 = true;
            stack.push(adj);
        }

    }

    static Stack<Node> sccStack = new Stack<Node>();
    static int s = 0;
    static int t = 0;
    static HashMap<Integer,Integer> finish = new HashMap<Integer,Integer>();
    static HashMap<Integer,Integer> leader = new HashMap<Integer,Integer>();


    private static void dfs(Graph graph, Node root) {

        Stack nodesToVisit = new Stack<Node>();
        nodesToVisit.add(root);

        // el tema es este: marco  cuando visito , no cuando termina
        while (!nodesToVisit.isEmpty()) {
            Node pop = (Node) nodesToVisit.peek();
            leader.put(pop.key, s);
//            pop.marked2 = true;
            if (pop.marked) {
                nodesToVisit.pop();
                t = t +1;
                finish.put(pop.key, t);
            } else {
                pop.marked = true;
                for (Node node : pop.adjList) {
                    nodesToVisit.push(node);
                }
            }

        }

        //    list nodes_to_visit = {root};
        //   while( nodes_to_visit isn't empty ) {
        // currentnode = nodes_to_visit.first();
        //  nodes_to_visit.prepend( currentnode.children );
        //do something
//    }

        // given a root node , first push the root node , and start looking at its adjs
//        if (!root.marked) {
//            Stack<Node> stack = new Stack<Node>();
//            root.marked = true;
//            stack.push(root);
//            while ( root.genIter().hasNext()) {
//                Node node = root.genIter().next();
//                if (!node.marked) {
//                    node.marked = true;
//                    stack.push(node);
//                }
//            }

        // we start the normal recursion driven by the queue
//            while (!stack.isEmpty()) {
//                Node v = stack.peek();
//                Boolean isVFinished = true; // this keeps track of when already have seen all the nodes in v adj matrix
//                if (v.genIter().hasNext()) {
//                    Node w = v.genIter().next();
//                        if (!w.marked) { //error ?
//                            w.marked = true;
//                            isVFinished = false; // w is not marked that means that v was not finished
//                            stack.push(w);
//                        } else {
//                            stack.pop();   // we are done with v , we throw it away
//                            isVFinished = true;
//                        }
//                } else {
//                    stack.pop();
//                }
//                if (isVFinished) {
//                    sccStack.add(v);
//                }
//            }

    }
}
