package com.company.algorithmsCourse;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by federicobrubacher on 3/2/15.
 */
public class TwoSum {

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public BigInteger nextInt() throws IOException {
            return new BigInteger(next());
        }

        public Long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {



        HashSet<Long> results = new HashSet<Long>();
        HashSet<Long> inputs = new HashSet<Long>();


        Input in3 = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/61.txt")));


        for (int i = 0; i < 1000000; i++) {

            inputs.add(in3.nextLong());

        }


        /*
        La idea : x + y = -10000, ponele x = 1 ,  existe -10001 ?
                          -10000, ponele x = 5 , exsite -10005 ?
         */


        for (Long input : inputs) {
            for (long i = -10000; i <= 10000; i++) {
                Long comp = i -input;
                if (inputs.contains(comp) && comp != input) {
                    results.add(i);
                    break;
                }

            }

        }
        System.out.println(results.size());
    }

}
