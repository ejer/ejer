package com.company.algorithmsCourse;

import java.io.*;
import java.util.Arrays;

/**
 * Created by federicobrubacher on 2/6/15.
 */
public class QuickSortPartition {

    // la idea de este algoritmo es que tenemos dos punteros i, j que delimitan 2 zonas
    // p <p , >p, ..... entnonces delimita los menores que pivot , j delimita hasta donde llegamos particionando

    // third part was
     static int counter = 0;
    static int A[];
    public static int   partition(int l, int r) {
//        int pivot = A[r];  //use last elem as pivot
        swap(l,r);         // use last
//        swap (l,  findMedian(l, r-1));
        int pivot = A[l];
        int i = l+1;
        for (int j = l+1; j <= r; j++) { // if A[j] > pivot do nothing (just increment j)
            if (A[j] < pivot) {
                swap(j,i);
                i+=1;
            }
        }

        swap(l, i-1);
        return i-1;  // return the place where the pivot was locted by the end of the run
    }

    private static int findMedian(int l, int r) {
        int middle = (r+l)/2;
        int[] values = new int[] { A[l], A[r], A[middle]};
        Arrays.sort(values);
        int middleVal = values[1];
        if (A[l] == middleVal) {
            return l;
        } else if (A[middle] == middleVal) {
            return middle;
        } else {
            return r;
        }
    }

    public static void swap(int a, int b) {
        int temp = A[a];
        A[a] = A[b];
        A[b] = temp;
    }

    public static int quickSort(int l, int r) {
        if (r>l) {
            counter += (r-l);
            int pivot = partition(l, r);
            quickSort(l, pivot - 1);
            quickSort(pivot + 1, r);
        }
        return  0;
    }



    public static void main(String[] args) throws IOException {

        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/2.txt")));
        A = new int[10000];
        for (int i = 0; i < A.length; i++) {
            A[i] = in3.nextInt();
        }
        A = new int[] {5, 3, 9 , 2, 11 , 10, 4};
//        quickSort(0, A.length-1);
        partition(0,A.length -1);
        System.out.print(counter);
//        for (int i = 0; i < A.length; i++) {
//            System.out.print(A[i]);
//        }


    }


    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

}
