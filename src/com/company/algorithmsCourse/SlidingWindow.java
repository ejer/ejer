package com.company.algorithmsCourse;

import java.util.*;

/**
 * Created by federicobrubacher on 10/1/15.
 */
public class SlidingWindow {


    public static void main(String[] args) {
        int[] nums = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int k = 2;
        int[] max = getSlidingMaxes(nums, k);
        for (int i = 0; i < max.length; i++) {
            System.out.print(max[i] + " ");
        }

    }

    private static int[] getSlidingMaxes(int[] nums, int k) {
        List<Integer> maxes = new ArrayList<>();
        Deque<Integer> window = new LinkedList<>();


        for (int i = 0; i < nums.length; i++) {
            window.addFirst(nums[i]);
            int currMax = Integer.MIN_VALUE;

            if (i >= k-1) {
                Iterator<Integer> iterator = window.iterator();
                while (iterator.hasNext()) {
                    int curr = iterator.next();
                    if (curr > currMax) {
                        currMax = curr;
                    }

                }

                window.removeLast();
                maxes.add(currMax);
            }
        }

        int[] array = new int[maxes.size()];
        for (int i = 0; i < maxes.size(); i++) {
            array[i] = maxes.get(i);
        }
        return array;
    }
}
