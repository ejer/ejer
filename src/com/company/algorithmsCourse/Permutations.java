package com.company.algorithmsCourse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by federicobrubacher on 10/6/15.
 */
public class Permutations {



    private void collectPermutations(int[] nums, int start, List<Integer> curr, List<List<Integer>> results) {

        if (curr.size() == nums.length) {
            results.add(curr);
            return;
        }

        for (int i = 0; i <= curr.size(); i++) {
            List<Integer> newperm = new ArrayList<>(curr);
            newperm.add(i, nums[start]);
            collectPermutations(nums, start+1, newperm, results);
        }

    }

    private void collectPermutationsAl(ArrayList<Integer> nums, int start, List<Integer> curr, List<List<Integer>> results) {

        if (curr.size() == nums.size()) {
            results.add(curr);
            return;
        }

        for (int i = 0; i <= curr.size(); i++) {
            List<Integer> newperm = new ArrayList<>(curr);
            newperm.add(i, nums.get(start));
            collectPermutationsAl(nums, start + 1, newperm, results);
        }

    }

    public static void main(String[] args) {

        int[] test  =  {1,2,3};
        ArrayList<Integer> ls = new ArrayList<>();
        ls.add(1);
        ls.add(2);
        ls.add(3);

        Permutations p = new Permutations();
//        List<List<Integer>> permute = p.permute(test);
//
        List<List<Integer>> permute = p.permuteAl(ls);

        for(List<Integer> per : permute) {
            System.out.println("" +  per);
        }
    }

    public List<List<Integer>> permuteAl(ArrayList<Integer> nums) {

        List<List<Integer>> results = new ArrayList<>();

        collectPermutationsAl(nums, 0, new ArrayList<Integer>(), results);

        return results;
    }

    public List<List<Integer>> permute(int[] nums) {

        List<List<Integer>> results = new ArrayList<>();

        collectPermutations(nums, 0, new ArrayList<Integer>(), results);

        return results;
    }


    public static void test() {
        LinkedList<Integer> ll = new LinkedList<>();
        new ArrayList<Integer>(ll);

    }
}
