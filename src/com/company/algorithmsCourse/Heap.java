package com.company.algorithmsCourse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicobrubacher on 3/1/15.
 */


/* MAX-HEAPIFY operation:

Find location of largest value of:

A[ i ], A[ Left( i )] and A[ Right( i ) ]

If not A[ i ], max-heap property does not hold.

Exchange A[ i ] with the larger of the two children to preserve max-heap property.

Continue this process of compare/exchange down the heap until subtree rooted at i is a max-heap.

At a leaf, the subtree rooted at the leaf is trivially a max-heap. */




public class Heap {

    public Heap(int[] a) {
        A = a;
    }

    public Heap() {
    }

    int[] A = new int[10000];

    int size = 1;
    int levels = 1;

    public void doMax() {
        for (int i = 1; i <= levels; i++) {
            maxheapify(i);
        }
    }

    public void doMin() {
        for (int i = 1; i <= levels; i++) {
            minheapify(i);
        }
    }

    public void doMaxInv() {
        for (int i = levels; i >= 1; i--) {
            maxheapify(i);
        }
    }

    public void doMinInv() {
        for (int i = levels; i >= 1; i--) {
            minheapify(i);
        }
    }


    public void maxheapify(int root) {

        int l = 2*root;
        int r = 2*root +1;
        int size = A.length;

        int largest = 0;

        if (l < size && A[l] > A[root]) {
            largest = l;
        } else largest = root;
        if (r < size && A[r] > A[largest]) {
            largest = r;
        }
        if (largest != root) {
            int tmp = A[root];
            A[root] = A[largest];
            A[largest] = tmp;
            maxheapify(largest);
        }

    }

    public void minheapify(int root) {

        int l = 2*root;
        int r = 2*root +1;
        int size = A.length;

        int smallest = 0;

        if (l < size && A[l] < A[root]) {
            smallest = l;
        } else smallest = root;
        if (r < size && A[r] < A[smallest]) {
            smallest = r;
        }
        if (smallest != root) {
            int tmp = A[root];
            A[root] = A[smallest];
            A[smallest] = tmp;
            minheapify(smallest);
        }

    }

    public static void main(String[] args) {
        int[] a = new int[] {0, 16 , 4 , 10, 14, 7, 9, 3, 2, 8, 1};
        int levels = 3;
        Heap heap = new Heap(a);
        for (int i = 1; i <= levels; i++) {
            heap.maxheapify(i);
        }
        System.out.println("hello world");
    }

    public boolean isEmpty() {
        if (size == 1)
            return true;
        return false;
    }


    /* para pasar de level :
    *
    * primer nivel es : 1, 2 ... 3 , 4 ... 7, 8 ... 15
    * Los niveles empiezan en 2**level
    */
    public void addMin(int i1) {
        A[size] = i1;

        int p = (int) Math.pow(2, levels);

        if (size % p == 0 )
            levels++;
        size++;
        doMinInv();
    }

    public void addMax(int i1) {
        A[size] = i1;

        int p = (int) Math.pow(2, levels);

        if (size % p == 0 )
           levels++;
        size++;
        doMaxInv();
    }

    public int size() {
        return size;
    }

    public int peek() {
        return A[1];
    }

    public int pollMin() {
        int head =  A[1];
        A[1] = Integer.MAX_VALUE;
        for (int i = 1; i < size; i++) {
            A[i] = A[i+1];
        }
        size--;
        int p = (int) Math.pow(2, levels);
        if (size % p == 0 )
           levels--;
        doMin();
        return head;
    }

    public int pollMax() {
        int head =  A[1];
        A[1] = 0;
        for (int i = 1; i < size; i++) {
            A[i] = A[i+1];
        }
        size--;
        int p = (int) Math.pow(2, levels);
        if (size % p == 0 )
           levels--;
        doMax();
        return head;
    }
}
