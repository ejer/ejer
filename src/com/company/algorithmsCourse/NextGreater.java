package com.company.algorithmsCourse;

import com.company.graph.Stack;

import java.util.ArrayList;

/**
 * Created by federicobrubacher on 10/11/15.
 */
public class NextGreater {

    public int sqrt(int a) {
        if ( a == 0 ) return 0;
        if (a < 4) return 1;
        int beg = 2;
        int end = a/2;

        while (beg < end) {
            int med = (beg+end)/2;
            if (med*med == a) return beg;
            if(med*med < a) {
                beg = med;
            } else {
                end = med-1;
            }
        }

        return beg;


    }

    public ArrayList<Integer> nextGreater(ArrayList<Integer> a) {
        ArrayList<Integer> ls = new ArrayList<Integer>();

        if (a.size() == 1) {
            ls.add(-1);
            return ls;
        }
        Stack<Integer> s = new Stack<Integer>();
        s.push(a.get(0));

        for (int i = 1;  i < a.size(); i++) {
            while(s.size() > 0) {
                if (s.peek() > a.get(i)) {
                    break;
                }
                ls.add(s.pop());
            }

            s.push(a.get(i));

        }

        while (s.peek() != null) {
            ls.add(s.pop());
        }
        return ls;
    }

    public static int[] getNGE(int[] a) {
        Stack<Integer> s = new Stack<Integer>();

        s.push(a[0]);

        int[] result = new int[a.length];
        for (int i = 1; i < a.length; i++) {
            if (s.size() > 0) {
                while (s.size() > 0) {
                    if (a[s.peek()] > a[i]) {
                        break;
                    }
                    result[s.pop()] = a[i];
                }
            }
            s.push(i);
        }
        while (s.size() > 0) {
            result[s.pop()] =  -1;
        }
        ArrayList<Integer> result2 = new ArrayList<>();
        for(int res : result) {
            result2.add(res);
        }
        return result;
    }

    public static void main(String[] args) {
       int [] ts =  {40,50,11,32,55,68,75};
        int [] ts2 =  {  34, 35, 27, 42, 5, 28, 39, 20, 28 };
        ArrayList<Integer> ls = new ArrayList<>();
        for(int t : ts2) {
            ls.add(t);

        }

        NextGreater n = new NextGreater();
        n.sqrt(6);
//        n.nextGreater(ls);
        int[] nge = getNGE(ts2);
        System.out.print("hello");
    }
}
