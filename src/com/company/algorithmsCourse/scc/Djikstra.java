package com.company.algorithmsCourse.scc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Created by federicobrubacher on 2/23/15.
 * Algorithm is from source s
 * graph { every edge }
 * known = {s}
 * unknown = graph - known
 * while (  unknown != {t} )
 * {
 * <p/>
 * smallest = Int.max;
 * for (a in known ) { or a a.pop
 * <p/>
 * for (i in edges(a, x)
 * smallest = Math.min(smallest, weight(a,x))
 * }
 * <p/>
 * known.add(smallest);
 * }
 * }
 */
public class Djikstra {

    public static Graph normal = new Graph();

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n".indexOf(c) == -1) {
                    sb.append((char) c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char) c);
            }
            return sb.toString();
        }

        public HashMap<Integer, Collection<Integer>> adjList() throws IOException {

            HashMap<Integer, Collection<Integer>> adjLists = new HashMap<Integer, Collection<Integer>>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int head = Integer.parseInt(nbrs[0]);
                    List<Integer> adjs = new ArrayList<Integer>();
                    for (int i = 1; i < nbrs.length; i++) {
                        adjs.add(Integer.parseInt(nbrs[i]));
                    }
                    adjLists.put(head, adjs);
                }
            }
            return adjLists;
        }




        public void generateGraph(Graph graph, List<Integer> startVertices, List<Integer[]> nodes, List<Integer[]> weights) {

            for (int i = 0; i < startVertices.size(); i++) {

                Integer[] adjNodes = nodes.get(i);
                Integer[] adjWeights = weights.get(i);
                Vertex[] adjList = new Vertex[adjNodes.length];
                WeightedEdge[] adjListEdges = new WeightedEdge[adjNodes.length];
                Vertex from = graph.get(startVertices.get(i));
                Boolean add = false;
                if (from == null) {
                    from = new Vertex(startVertices.get(i));
                    add = true;
                }

                for (int j = 0; j < adjNodes.length; j++) {
                    Vertex into = graph.get(adjNodes[j]);
                    Boolean add1 = false;
                    if (into == null) {
                        add1 = true;
                        into = new Vertex(adjNodes[j]);
                    }

                    adjList[j] = into;

                    WeightedEdge weightedEdge = new WeightedEdge(from, into, adjWeights[j]);

                    adjListEdges[j] = weightedEdge;

                    if ( add1 ) {
                        graph.addVertex( into );
                    }


                }

                from.adjList = Arrays.asList(adjList);
                from.outgoing = Arrays.asList(adjListEdges);
                if (add) {
                    graph.addVertex(from);
                }
            }

        }

        public void readTuples() throws IOException {
            List<Integer[]> connectList = new ArrayList<Integer[]>();
            List<Integer[]> connectWeightsList = new ArrayList<Integer[]>();
            List<Integer> startVertexList = new ArrayList<Integer>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split("\\t");
                    int a = Integer.parseInt(nbrs[0]);

                    Integer connectedV[] = new Integer[nbrs.length-1];
                    Integer connectedW[] = new Integer[nbrs.length-1];

                    for (int i = 1; i < nbrs.length; i++) {
                        String vertices[] = nbrs[i].split(",");

                        connectedV[i-1] = Integer.parseInt(vertices[0]);
                        connectedW[i-1] = Integer.parseInt(vertices[1]);
                        connectList.add(connectedV);
                        connectWeightsList.add(connectedW);
                        startVertexList.add(a);
                    }
                }
            }
            generateGraph(normal, startVertexList, connectList, connectWeightsList);
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static class Graph<V extends Edge<E>, E extends Vertex<V>> {

        ArrayList<Vertex> vertexes = new ArrayList<Vertex>();
        HashMap<Integer, Vertex> integerToNode = new HashMap<Integer, Vertex>();


        public Graph() {
        }

        public void addVertex(Vertex vertex) {
            vertexes.add(vertex);
            integerToNode.put(vertex.key, vertex);
            vertex.adj = vertex.adjList.iterator();
        }


        public boolean contains(Integer integer) {
            if (integerToNode.containsKey(integer))
                return true;
            return false;
        }

        public Vertex get(Integer integer) {
            return integerToNode.get(integer);
        }

        public int size() {
            return this.vertexes.size();
        }

        public Edge[] getEdgesWith(int s) {
            Vertex vertex = integerToNode.get(s);
            return (Edge[]) vertex.getOutgoing();
        }

        public WeightedEdge[] getWeightedEdgesWith(int s) {
            Vertex vertex = integerToNode.get(s);
            return (WeightedEdge[]) vertex.getOutgoing();
        }
    }

    public static class Vertex<Edge> implements Comparable<Vertex> {
        public int key;
        public List<Vertex> adjList;
        public boolean marked = false;
        public boolean marked2 = false;
        public List<Edge> outgoing = new ArrayList<Edge>();
        public List<Edge> incoming = new ArrayList<Edge>();

        public Iterator<Vertex> adj;

        public Edge[] getOutgoing() {
            return (Edge[]) outgoing.toArray();
        }

        public Edge[] getIncoming() {
            return (Edge[]) incoming.toArray();
        }

        public void addIncoming(Edge edge) {
            incoming.add(edge);
        }

        public void addOutgoing(Edge edge) {
            outgoing.add(edge);
        }

        public void setIncoming(List<Edge> incoming) {
            this.incoming = incoming;
        }

        public void setOutgoing(List<Edge> outgoing) {
            this.outgoing = outgoing;
        }

        public Vertex(int key, List<Vertex> adjList) {
            this.adjList = adjList;
            this.key = key;
        }

        public Vertex(Integer integer) {
            this.key = integer;
            this.adjList = new ArrayList<Vertex>();
        }

        public void addVertexToList(Vertex adj) {
            adjList.add(adj);
        }

        public Iterator<Vertex> genIter() {
            if (adj == null) {
                adj = adjList.iterator();
            }
            return adj;
        }

        @Override
        public int compareTo(Vertex o) {
            if (this.key >= o.key)
                return 1;
            return -1;
        }
    }

    public static class Edge<Vertex> {
        public Vertex from;
        public Vertex into;

        public Vertex getFrom() {
            return from;
        }

        public Vertex getInto() {
            return into;
        }

        public Edge(Vertex from, Vertex into) {
            this.from = from;
            this.into = into;
        }
    }

    public static class WeightedEdge extends Edge {

        Integer weight;

        public WeightedEdge(Vertex from, Vertex into, Integer weight) {
            super(from, into);
            this.weight = weight;
        }
    }

    /* Dijkstra is : we star from s
    find the closest edge not in the set
    add it (vnext), and compute new dikstra greedy param :
    dist[vnext] = dist[vnext] + w(u, vnext)
    add vnext to the set

     */
    public static void Dijkstra(Graph G, int s, int t) {

        int n = G.size();
        Set<Integer> known = new HashSet<Integer>();
        known.add(s);
        Integer[] dist = new Integer[n];


        for (int i = 0; i < n; i++) {
            dist[i] = Integer.MAX_VALUE;
        }

        WeightedEdge[] weightedEdges = G.getWeightedEdgesWith(s);

        for (WeightedEdge edge : weightedEdges) {
            Vertex in = (Vertex) edge.into;
            dist[in.key-1] = edge.weight;
        }

        int last = s;
        int newMinDist = 0;
        while (last != t) {
            newMinDist = Integer.MAX_VALUE;
            int vnext = 0;
            for (int i = 0; i < dist.length; i++) {
                int distI = dist[i];
                if (distI < newMinDist && !known.contains(i+1))   {
                    newMinDist = distI;
                    vnext = G.get(i+1).key;
                }
            }

            WeightedEdge[] edgesWithVnext = G.getWeightedEdgesWith(vnext);
            for (int i = 0; i < edgesWithVnext.length; i++) {
                WeightedEdge e = edgesWithVnext[i];
                // update the dist[] starting from vnext
//                Vertex from = (Vertex) e.from;
                Vertex into = (Vertex) e.into;
                dist[into.key-1] = Math.min(dist[into.key-1] , newMinDist +  e.weight);


            }
            last = vnext;
            known.add(vnext);


        }

        System.out.print(newMinDist + ",");
    }

    public static void main(String[] args) throws IOException {
        Input in3 = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/5.txt")));
        in3.readTuples();
        int[] vals = new int[] { 7,37,59,82,99,115,133,165,188,197 };
        for (int i = 0; i < vals.length; i++) {
            Dijkstra(normal, 1, vals[i]);
        }


    }

}
