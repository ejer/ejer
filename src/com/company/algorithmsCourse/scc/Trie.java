package com.company.algorithmsCourse.scc;

/**
 * Created by federicobrubacher on 9/22/15.
 */
class TrieNode {
    // Initialize your data structure here.

    TrieNode[] nodes;

    String prefix = "";

    boolean isEnd = false;

    public void setIsEnd(boolean val) {
        this.isEnd = val;
    }

    public boolean getIsEnd() {
        return isEnd;
    }

    public TrieNode insertSibling(char c) {
        int pos = c - 'a';
        TrieNode sib;
        if (this.nodes[pos] == null) {sib = new TrieNode(); } else {
            sib = this.nodes[pos];
        }
        sib.prefix = this.prefix + c;
        nodes[pos] = sib;
        return sib;
    }

    public TrieNode() {
        nodes = new TrieNode[26];
    }
}

public class Trie {

    public static void main(String[] args) {

        Trie trie = new Trie();
        trie.insert("app");trie.insert("apple");trie.insert("beer");trie.insert("add");trie.insert("jam");trie.insert("rental");
        boolean add1 = trie.search("app");
        System.out.print(add1);

    }


    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    // Inserts a word into the trie.
    public void insert(String word) {
        TrieNode node = root;
        for (char letter : word.toCharArray()) {
            node = node.insertSibling(letter);
        }
        node.setIsEnd(true);

    }

    // Returns if the word is in the trie.
    public boolean search(String word) {
        TrieNode node = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            int pos = c - 'a';

            if (node.nodes[pos] != null) {
                node = node.nodes[pos];
            } else {
                return false;
            }
        }

        return true;

    }

    // Returns if there is any word in the trie
    // that starts with the given prefix.
    public boolean startsWith(String prefix) {
        TrieNode node = root;
        for (int i = 0; i < prefix.length(); i++) {
            char c = prefix.charAt(i);
            int pos = c - 'a';

            if (node.nodes[pos] != null) {
                node = node.nodes[pos];
            } else {
                return false;
            }
        }

        if(node.getIsEnd()) {
            return true;
        } else {
            return false;
        }
    }
}

// Your Trie object will be instantiated and called as such:
// Trie trie = new Trie();
// trie.insert("somestring");
// trie.search("key");
