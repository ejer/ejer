package com.company.algorithmsCourse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicobrubacher on 10/9/15.
 */
public class searchRange {

    public static void main(String[] args) {
        int[] a = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
        ArrayList<Integer> ls = new ArrayList<>();
        for(int a1 : a) {
            ls.add(a1);
        }
        int b = 10;
        ArrayList<Integer> integers = searchRange(ls, b);
        System.out.println(integers);

    }

    public static ArrayList<Integer> searchRange(final List<Integer> a, int b) {

        ArrayList<Integer> ls = new ArrayList<>();

        if (a.size() == 1 && a.get(0) == b) {
            ls.add(0);
            ls.add(0);
            return ls;
        }

        int beg = 0;
        int end = a.size()-1;
        int med = (beg+end)/2;
        boolean found = false;
        while ( beg < end ) {
            med = (beg+end)/2;
            if ( b > a.get(med) ) {
                beg = med + 1;
            } else if (b < a.get(med) ) {
                end = med;
            } else {
                found = true;
                break;
            }
        }

        if (found) {
            int i = med+1;
            int left = med;
            int right = med;
            while (a.get(i-1) == b) {
                i++;
                right++;
                if (i > a.size()-1) break;
            }
            i = med-1;
            while (a.get(i-1) == b) {
                i--;
                left--;
                if (i < 0) break;
            }

            ls.add(left);
            ls.add(right);
            return ls;


        } else {
            ls.add(-1);
            ls.add(-1);
            return ls;
        }

    }

}
