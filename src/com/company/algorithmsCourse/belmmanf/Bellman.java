package com.company.algorithmsCourse.belmmanf;

import sun.jvm.hotspot.memory.EdenSpace;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by federicobrubacher on 4/23/15.
 */
public class Bellman {

    public class Edge{

        public Vertex to;
        public Vertex from;
        public int weight;

        public Edge(Vertex v1, Vertex v2) {
            from = v1;
            to = v2;
        }

    }

    public class Vertex {
        public int value;
        public ArrayList<Edge> edges;

        public Vertex(int value) {
            edges = new ArrayList<Edge>();
            this.value = value;
        }

//        @Override
//        public boolean equals(Object o) {
//            return (((Vertex)o).value == this.value);
//        }
//
//
//        @Override
//        public int hashCode() {
//            return value;
//        }
    }


    static HashMap<Integer, Vertex> to = new HashMap<Integer, Vertex>();
    static HashMap<Vertex, Vertex> from = new HashMap<Vertex, Vertex>();
    static int[][] A;

    static int m;
    static int n;

    public void bellmanFord() {

        for (int i = 1; i < m; i++) {


            for (int v = 0; v < n; v++) {

                if (to.containsKey(v)) {

                    Vertex v1 = to.get(v);

                    int minInvertex = 20000000;

                    for (Edge e : v1.edges) {
                        Vertex w1 = e.from;
                        int w = w1.value;
                        int weight = e.weight;
                        int iter = A[i - 1][w] + weight;
                        minInvertex  = iter < minInvertex ? iter : minInvertex;
                    }


                    A[i][v] =  Math.min(A[i-1][v], minInvertex);


                }
            }

        }

        System.out.println("Result " + A[m][n]);


    }


    public void read(Scanner scanner) {

        while (scanner.hasNext()) {
            int v1int = scanner.nextInt();
            Vertex v1;
            if (to.containsKey(v1int)) {
                v1 = to.get(v1int);
            } else {
                v1 = new Vertex(v1int);
            }
            int v2int = scanner.nextInt();
            Vertex v2;
            if (to.containsKey(v2int)) {
                v2 = to.get(v2int);
            } else {
                v2 = new Vertex(v2int);
            }
            Edge edge = new Edge(v1, v2);
            edge.weight = scanner.nextInt();
            v2.edges.add(edge);
            to.put(v2.value, v2);

        }



    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera2/bellmanf.txt.test")));
        m = scanner.nextInt();
        n = scanner.nextInt();
        
        A = new int[m][n];


        Bellman bellman = new Bellman();
        bellman.read(scanner);

        int s = 0;

        for (int i = 0; i < m; i++) {
            if ( i == s ) {
                A[0][i] = 0;
            } else {
                A[0][i] = 100000;
            }
        }

        bellman.bellmanFord();



    }

}
