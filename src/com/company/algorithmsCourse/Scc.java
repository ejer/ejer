package com.company.algorithmsCourse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Created by federicobrubacher on 2/9/15.
 * Let G be a directed graph and S be an empty stack.
 While S does not contain all vertices:
 Choose an arbitrary vertex v not in S. Perform a depth-first search starting at v. Each time that depth-first search finishes
    expanding a vertex u, push u onto S.
 Reverse the directions of all arcs to obtain the transpose graph.
 While S is nonempty:
 Pop the top vertex v from S. Perform a depth-first search starting at v in the transpose graph.
    The set of visited vertices will give the strongly connected component containing v;
    record this and remove all these vertices from the graph G and the stack S.
    Equivalently, breadth-first search (BFS) can be used instead of depth-first search.
 */
public class Scc {

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public HashMap<Integer,Collection<Integer>> adjList() throws IOException {

            HashMap<Integer, Collection<Integer>> adjLists = new HashMap<Integer, Collection<Integer>>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int head = Integer.parseInt(nbrs[0]);
                    List<Integer> adjs = new ArrayList<Integer>();
                    for (int i = 1; i < nbrs.length; i++) {
                        adjs.add(Integer.parseInt(nbrs[i]));
                    }
                    adjLists.put(head, adjs);
                }
            }
            return adjLists;
        }


        public static Map<Integer, Set<Integer>> normal = new HashMap<Integer, Set<Integer>>();
        public static Map<Integer, Set<Integer>> reverse = new HashMap<Integer, Set<Integer>>();


        public void generateGraph(Map<Integer, Set<Integer>> graph, List<Integer[]> nodes) {
            for (Integer[] node : nodes) {
                if (graph.containsKey(node[0])) {
                    Set<Integer> adjNodes = graph.get(node[0]);
                    adjNodes.add(node[1]);
                    graph.put(node[0], adjNodes);
                } else {
                    HashSet<Integer> adjNodes = new HashSet<Integer>();
                    adjNodes.add(node[1]);
                    graph.put(node[0], adjNodes);
                }
            }

        }

        public void readTuples() throws IOException {
            List<Integer[]> nodes = new ArrayList<Integer[]>();
            List<Integer[]> reverseList = new ArrayList<Integer[]>();

            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                } else {
                    String nbrs[] = line.split(" ");
                    int a = Integer.parseInt(nbrs[0]);
                    int b = Integer.parseInt(nbrs[1]);
                    Integer[] node = new Integer[]{a, b};
                    Integer[] rev = new Integer[]{b, a};
                    reverseList.add(rev);
                    nodes.add(node);
                }
            }
            generateGraph(normal, nodes);
            generateGraph(reverse, reverseList);
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    public static void main(String[] args) throws IOException {
        Input    in3 = new Input( new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/4test.txt")));
        in3.readTuples();
        dfs(in3.normal);
        dfsInverse(in3.reverse);
    }

    private static void dfsInverse(Map<Integer, Set<Integer>> reverse) {
        while (!s.empty()) {
            Integer parent = s.pop();
            System.out.println(doDfsReverse(reverse, parent, 0));
        }
    }

    public static HashSet<Integer> visitedRev = new HashSet<Integer>();

    private static int doDfsReverse(Map<Integer, Set<Integer>> reverse, Integer parent, int visitedRevCount) {
        Set<Integer> adjNodes = reverse.get(parent);
        visitedRev.add(parent); // mark the nodes we already visited
        visitedRevCount+=1;
        if (adjNodes.isEmpty())
            return 1;

        for (Integer adjNode : adjNodes) {
            if (visitedRev.contains(adjNode)) { //exit condition , basicallay means that if another node is in another SCC it cannot be in this SCC (false)
                s.removeAll(visitedRev);
                return visitedRevCount; // get the size of the SCC
            } else   {
                return doDfsReverse(reverse, adjNode, visitedRevCount);
            }
        }
        return visitedRev.size();
    }

    public static Stack<Integer> s = new Stack<Integer>();

    private static void dfs(Map<Integer, Set<Integer>> graph) {
        for (Integer key : graph.keySet()) {
            if (!s.contains(key)) {
              doDfs(graph, key);
            }
        }
    }
    static Set<Integer> visited = new HashSet<Integer>();

    private static void doDfs(Map<Integer, Set<Integer>> graph, Integer parent) {
        Set<Integer> adjNodes = graph.get(parent);
        if (visited.contains(parent))
            return;
        visited.add(parent);
        if (adjNodes != null) {
            for (Integer adjNode : adjNodes) {
                doDfs(graph, adjNode);
            }
            s.add(parent);
        } else {
            s.add(parent);
        }
    }




}
