package com.company.algorithmsCourse;

import java.io.*;
import java.util.*;

/**
 * Created by federicobrubacher on 2/27/15.
 */
public class MedianMaintenance {

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    Heap hmax = new Heap();
    Heap hlow = new Heap();
    static List<Integer> medians = new ArrayList<Integer>();

    public void execute() throws IOException {


        for (int i = 0; i < hmax.A.length; i++) {

            hmax.A[i] = Integer.MAX_VALUE;
        }

        for (int i = 0; i < hlow.A.length; i++) {

            hlow.A[i] = Integer.MIN_VALUE;
        }

        Input in3 = new Input(new BufferedReader(new FileReader("/Users/federicobrubacher/testcases/coursera1/62.txt")));

        for (int i = 0; i < 10000; i++) {
            int i1 = in3.nextInt();

            int median = 0;

            if (hlow.isEmpty()){
                hlow.addMax(i1);
            } else {

               if (i1 > hlow.peek()) {
                    hmax.addMin(i1); // como es mas grande que la median , tengo que insertarlo en el menor de los mayores
                } else {
                    hlow.addMax(i1);
                }
            }


            boolean even = true;

            int size_diff = hlow.size() - hmax.size();

            // Maintain invariant

            int size_diff_abs = Math.abs(size_diff);

            if (size_diff == 0) {
            }
            else if (size_diff > 1) {
                for (int h = 0; h < size_diff_abs-1; h++) {
                    hmax.addMin(hlow.pollMax());
                }
            }
            else if (size_diff < -1) {
                for (int h = 0; h < size_diff_abs-1; h++) {
                    hlow.addMax(hmax.pollMin());
                }
            } else  if (size_diff > 0) {
                even = false;
                median = hlow.peek();

            } else if (size_diff < 0) {
                even = false;
                median = hmax.peek();
            }

            if (even) {
                median = hlow.peek();
            }


//            System.out.println("ingreso : "  + i1);
//            print(hlow);
//            System.out.print("|||");
           medians.add(median);
//           System.out.print(median);
//            System.out.print("|||");
//          print(hmax);
//            System.out.print("\n");


        }



    }

    private void print(Heap heap) {
        for (int j = 1; j < heap.size+1; j++) {
            int i = heap.A[j];
//            if (i == Integer.MAX_VALUE || i == Integer.MIN_VALUE)
//                break;

            System.out.print(" " + i + " ");
        }
    }


    public static void main(String[] args) {

        MedianMaintenance medianMaintenance;
        medianMaintenance = new MedianMaintenance();
        try {
            medianMaintenance.execute();
                int sum = 0;
            for (Integer median : medians) {

                sum = sum + median;

            }


            System.out.println("suma: " +  sum % 10000);
        } catch (IOException e) {

        }

    }

}
