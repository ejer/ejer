package com.company.graph;

/**
 * Created by federicobrubacher on 12/29/14.
 */
// helper linked list class
public  class Node<Item> {
    public Item item;
    public Node<Item> next;
//    public Integer itemWeight;
}
