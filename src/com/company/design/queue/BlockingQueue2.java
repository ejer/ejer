package com.company.design.queue;

import java.util.concurrent.atomic.AtomicBoolean;

public class BlockingQueue2<T> extends ArrayQueueFixedSize<T> {


    AtomicBoolean pushFlag = new AtomicBoolean(true);
    AtomicBoolean popFlag = new AtomicBoolean(true);

    @Override
    public void push(T value) {
        try {
            synchronized (pushFlag) {
                if (size >= N) {
                    pushFlag.wait();
                }
                super.push(value);
                popFlag.notifyAll();
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }



}