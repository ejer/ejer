package com.company.design.queue;


/**
 * Created by federicobrubacher on 10/1/15.
 */

public class ArrayQueueFixedSize<T> implements Queue<T> {

    protected int N = 10;

    private T[] array = (T[]) new Object[N];

    protected int head = 0;
    protected int tail = 0;
    protected int size = 0;


    @Override
    public void push(T item) {
        if (size < N) {
            array[tail] = item;
            tail = (tail + 1) % N;
            size++;
        } else {
            System.err.println("Queue is full");
        }


    }

    @Override
    public T pop() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }
}
