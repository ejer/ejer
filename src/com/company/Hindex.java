package com.company;

import java.util.Arrays;

/**
 * Created by federicobrubacher on 9/7/15.
 */
public class Hindex {
    public int hIndex(int[] citations) {
        Arrays.sort(citations);

        int H = 0;
        int L = 0;

        int length = citations.length;

        for (int i =  citations.length-1; i >= 0; i++) {
            H = citations[i];

            if ( L < H) {
                L = L+1;
            }

        }

        return Math.min(L,H);
    }

    public static void main(String[] args) {

        int[] args1 = {0, 1};

        Hindex index = new Hindex();
        int i = index.hIndex(args1);
        System.out.print(i);


    }

}
