/**
 * Created by federicobrubacher on 8/8/15.
 */
public class Candies2 {

    static int N;

    public static int giveCandies(int[] grades, int childId, int prevCandy) {


        if (childId > N) {
            return 0;
        }

        if (grades[childId - 1] < grades[childId]) {

            int min = Integer.MAX_VALUE;
            for (int i = prevCandy + 1; i <= grades[childId]; i++) {

                int curr = giveCandies(grades, childId + 1, i) + i;
                if (curr < min) {
                    min = curr;
                }
            }
            return min;


        }

        int min = Integer.MAX_VALUE;
        for (int i = 1; i <= N; i++) {


            int curr = giveCandies(grades, childId + 1, i) + i;
            if (curr < min) {
                min = curr;
            }
        }

        return min;

    }

    public static void main(String[] args) {

        N = 3;
        int[] grades = {0, 1, 2, 2};
        int sult = giveCandies(grades, 1, 0);
        System.out.print(sult);

    }
}
